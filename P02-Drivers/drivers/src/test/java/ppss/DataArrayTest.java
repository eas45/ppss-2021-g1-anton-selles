package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Tag("DataArrayTest")
class DataArrayTest {

  DataArray resultadoEsperado;
  DataArray resultadoReal;
  int elementoABorrar;

  @Test
  void F1_delete() {
    int[] coleccionEsperada = {1, 3, 7};
    int[] coleccion = {1, 3, 5, 7};
    elementoABorrar = 5;
    resultadoEsperado = new DataArray(coleccionEsperada);
    resultadoReal = new DataArray(coleccion);

    // Si salta la excepción, no debe comparar el resultado
    assertDoesNotThrow(() -> resultadoReal.delete(elementoABorrar));
    assertAll("Test F1",
            () -> assertArrayEquals(resultadoEsperado.getColeccion(), resultadoReal.getColeccion()),
            () -> assertEquals(resultadoEsperado.size(), resultadoReal.size()));
  }

  @Test
  void F2_delete() {
    int[] coleccionEsperada = {1, 3, 5, 7};
    int[] coleccion = {1, 3, 3, 5, 7};
    elementoABorrar = 3;
    resultadoEsperado = new DataArray(coleccionEsperada);
    resultadoReal = new DataArray(coleccion);

    assertDoesNotThrow(() -> resultadoReal.delete(elementoABorrar));
    assertAll("Test F2",
            () -> assertArrayEquals(resultadoEsperado.getColeccion(), resultadoReal.getColeccion()),
            () -> assertEquals(resultadoEsperado.size(), resultadoReal.size()));
  }

  @Test
  void F3_delete() {
    int[] coleccionEsperada = {1, 2, 3, 5, 6, 7, 8, 9, 10};
    int[] coleccion = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    elementoABorrar = 4;
    resultadoEsperado = new DataArray(coleccionEsperada);
    resultadoReal = new DataArray(coleccion);

    assertDoesNotThrow(() -> resultadoReal.delete(elementoABorrar));
    assertAll("Test F3",
            () -> assertArrayEquals(resultadoEsperado.getColeccion(), resultadoReal.getColeccion()),
            () -> assertEquals(resultadoEsperado.size(), resultadoReal.size()));
  }

  @Test
  void F4_delete() {
    // Es necesario inicializarlo aunque esté vacío
    int[] coleccion = {};
    elementoABorrar = 8;
    resultadoReal = new DataArray(coleccion);

    Exception exception = assertThrows(DataException.class,
            () -> resultadoReal.delete(elementoABorrar));
    assertEquals("No hay elementos en la colección", exception.getMessage());
  }

  @Test
  void F5_delete() {
    int[] coleccion = {1, 3, 5, 7};
    elementoABorrar = -5;
    resultadoReal = new DataArray(coleccion);

    Exception exception = assertThrows(DataException.class,
            () -> resultadoReal.delete(elementoABorrar));
    assertEquals("El valor a borrar debe ser > cero", exception.getMessage());
  }

  @Test
  void F6_delete() {
    int[] coleccion = {};
    elementoABorrar = 0;
    resultadoReal = new DataArray(coleccion);

    Exception exception = assertThrows(DataException.class,
            () -> resultadoReal.delete(elementoABorrar));
    assertEquals("Colección vacía. Y el valor a borrar debe ser > cero", exception.getMessage());
  }

  @Test
  void F7_delete() {
    int[] coleccion = {1, 3, 5, 7};
    elementoABorrar = 8;
    resultadoReal = new DataArray(coleccion);

    Exception exception = assertThrows(DataException.class,
            () -> resultadoReal.delete(elementoABorrar));
    assertEquals("Elemento no encontrado", exception.getMessage());
  }
}