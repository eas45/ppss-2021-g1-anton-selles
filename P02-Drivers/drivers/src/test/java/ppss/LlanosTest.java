package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class LlanosTest {

  ArrayList<Integer> lecturas;
  Tramo resultadoEsperado;
  Tramo resultadoReal;

  Llanos llano;

  @BeforeEach void inicializar() {
    resultadoReal = new Tramo();
    llano = new Llanos();
  }

  @Tag("TablaA")
  @Test
  void C1A_buscarTramoLlanoMasLargo() {
    // Inicialización
    lecturas = new ArrayList<>(
            Arrays.asList(10, 10, 10)
    );
    resultadoEsperado = new Tramo(0, 2);
    resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

    // Comprobación atributo a atributo
    assertAll("Test C1A",
            () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen()),
            () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud()));
  }

  @Tag("TablaA")
  @Test
  void C2A_buscarTramoMasLargo() {
    lecturas = new ArrayList<>(
            Arrays.asList(10)
    );
    resultadoEsperado = new Tramo();
    resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

    // Comprobación con redefinición del método equals()
    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Tag("TablaB")
  @Test
  void C1B_buscarTramoMasLargo() {
    lecturas = new ArrayList<>(
            Arrays.asList(-1)
    );
    resultadoEsperado = new Tramo();
    resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

    assertAll("Test C1B",
            () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen(), "Error en <origen>"),
            () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud(), "Error en <longitud>"));
  }

  @Tag("TablaB")
  @Test
  void C2B_buscarTramoMasLargo() {
    lecturas = new ArrayList<>(
            Arrays.asList(-1, -1, -1, -1)
    );
    resultadoEsperado = new Tramo(0, 3);
    resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

    assertAll("Test C2B",
            () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen(), "Error en <origen>"),
            () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud(), "Error en <longitud>"));
  }

  @Tag("TablaB")
  @Test
  void C3B_buscarTramoMasLargo() {
    lecturas = new ArrayList<>(
            Arrays.asList(120, 140, -10, -10, -10)
    );
    resultadoEsperado = new Tramo(2, 2);
    resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

    assertAll("Test C3B",
            () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen(), "Error en <origen>"),
            () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud(), "Error en <longitud>"));
  }

  @Test
  void C1C_buscarTramoMasLargo() {
    lecturas = new ArrayList<>(
//            Arrays.asList(120, 140, -10, -10, -10)
    );
    resultadoEsperado = new Tramo(0, 0);
    resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

    assertAll("Test C1C",
            () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen(), "Error en <origen>"),
            () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud(), "Error en <longitud>"));
  }

}