package ppss;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@Tag("Parameterized")
class LlanosParamTest {

  Llanos resultadoReal = new Llanos();

  @ParameterizedTest
  @MethodSource("casosDePrueba")
  void buscarTramoLlanoMasLargo(ArrayList<Integer> lecturas, Tramo resultadoEsperado) {
    assertAll("Test parametrizado con " + lecturas,
            () -> assertEquals(resultadoEsperado.getOrigen(),
                              resultadoReal.buscarTramoLlanoMasLargo(lecturas).getOrigen(),
                    () -> generateFailureMessage_Origen(lecturas, resultadoEsperado)),
            () -> assertEquals(resultadoEsperado.getLongitud(),
                              resultadoReal.buscarTramoLlanoMasLargo(lecturas).getLongitud(),
                    () -> generateFailureMessage_Longitud(lecturas, resultadoEsperado)));
  }

  private static Stream<Arguments> casosDePrueba() {
    return Stream.of(
            Arguments.of(new ArrayList<>(Arrays.asList(10, 10, 10)), new Tramo(0, 2)),
            Arguments.of(new ArrayList<>(Arrays.asList(10)), new Tramo()),
            Arguments.of(new ArrayList<>(Arrays.asList(-1)), new Tramo()),
            Arguments.of(new ArrayList<>(Arrays.asList(-1, -1, -1, -1)), new Tramo(0, 3)),
            Arguments.of(new ArrayList<>(Arrays.asList(120, 140, -10, -10, -10)), new Tramo(2, 2))
    );
  }

  // Generador de mensaje de error de ORIGEN
  private String generateFailureMessage_Origen(ArrayList<Integer> lecturas, Tramo resultadoEsperado) {

    return "En las lecturas " + lecturas + " el origen está en el km " + resultadoEsperado.getOrigen();
  }

  // Generador de mensaje de error de LONGITUD
  private String generateFailureMessage_Longitud(ArrayList<Integer> lecturas, Tramo resultadoEsperado) {

    return "En las lecturas " + lecturas + " la longitud de la llanura es de " + resultadoEsperado.getLongitud() + " km";
  }
}