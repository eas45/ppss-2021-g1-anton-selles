package ppss;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MatriculaTest {

  int edad;
  boolean familiaNumerosa;
  boolean repetidor;
  float resultadoReal, resultadoEsperado;
  Matricula matricula;

  @BeforeEach void inicializar() {
    matricula = new Matricula();
  }

  @Test
  void C1_calculaTasaMatricula() {
    // Preparación de datos de entrada
    edad = 19;
    familiaNumerosa = false;
    repetidor = true;
    // Resultado esperado según la especificación
    resultadoEsperado = 2000.00f;
    resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor); // Se llama al SUT
    // Comparación de resultados
    assertEquals(resultadoEsperado, resultadoReal, 0.002f);
  }

  @Test
  void C2_calculaTasaMatricula() {
    edad = 20;
    familiaNumerosa = false;
    repetidor = false;
    resultadoEsperado = 500.00f;
    resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

    assertEquals(resultadoEsperado, resultadoReal, 0.002f);
  }

  @Test
  void C3_calculaTasaMatricula() {
    edad = 23;
    familiaNumerosa = true;
    repetidor = true;
    resultadoEsperado = 250.00f;
    resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

    assertEquals(resultadoEsperado, resultadoReal, 0.002f);
  }

  @Test
  void C4_calculaTasaMatricula() {
    edad = 67;
    familiaNumerosa = false;
    repetidor = true;
    resultadoEsperado = 250.00f;
    resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

    assertEquals(resultadoEsperado, resultadoReal, 0.002f);
  }

  @Test
  void C5_calculaTasaMatricula() {
    edad = 52;
    familiaNumerosa = true;
    repetidor = false;
    resultadoEsperado = 400.00f;
    resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

    assertEquals(resultadoEsperado, resultadoReal, 0.002f);
  }

  // Es un caso de prueba que debería de fallar
  @Test
  void C6_calculaTasaMatricula() {
    edad = 60;
    familiaNumerosa = true;
    repetidor = true;
    resultadoEsperado = 400.00f;
    resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

    assertEquals(resultadoEsperado, resultadoReal, 0.002f);
  }
}