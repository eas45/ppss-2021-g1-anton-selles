package ppss;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@Tag("Parameterized")
class MatriculaParamTest {

  Matricula matricula = new Matricula();

  @ParameterizedTest(name = "TEST PARAMETRIZADO con edad {0}")
  @MethodSource("casosDePrueba")
  void calculaTasaMatricula(int edad, boolean familiaNumerosa, boolean repetidor, float resultadoEsperado) {
    assertEquals(resultadoEsperado, matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor), 0.002f,
            () -> generateFailureMessage(edad, familiaNumerosa, repetidor, resultadoEsperado));
  }

  // Generador de resultados esperados
  private static Stream<Arguments> casosDePrueba() {
    return Stream.of( // edad - familiaNumerosa - repetidor - resultadoEsperado
        Arguments.of(19, false, true, 2000.00f),
        Arguments.of(20, false, false, 500.00f),
        Arguments.of(23, true, true, 250.00f),
        Arguments.of(67, false, true, 250.00f),
        Arguments.of(52, true, false, 400.00f),
        Arguments.of(60, true, true, 400.00f)
    );
  }

  // Generador de mensaje de error
  private String generateFailureMessage(int edad, boolean familiaNumerosa, boolean repetidor, float resultadoEsperado) {

    String message = "Con " + edad + " años, ";

    if (familiaNumerosa) {
      message += "siendo ";
    } else {
      message += "sin ser ";
    }
    message += "familia numerosa y ";

    if (!repetidor) {
      message += "no ";
    }
    message += "habiendo repetido, la tasa debe ser de " + resultadoEsperado + " €";

    return message;
  }
}