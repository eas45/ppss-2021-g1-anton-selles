package ppss;

import java.util.ArrayList;

public class Llanos {
  public Tramo buscarTramoLlanoMasLargo(ArrayList<Integer> lecturas) {
    // Comienza por el primer dato (se sabe que el array no será null)
    int lectura_anterior;
    int longitud_tramo = 0, longitudMax_tramo = 0;
    int origen_tramo = -1, origen_tramoMax = -1;
    Tramo resultado = new Tramo();  //el origen y la longitud es CERO
    // Se inicializa la variable auxiliar
    Integer dato = null;

//        for(Integer dato:lecturas) {
    for (int i = 1; i < lecturas.size(); i++) {
      // Se recupera el dato de la posición i
      dato = lecturas.get(i);
      // Se recupera el elemento anterior
      lectura_anterior = lecturas.get(i - 1);
      if (lectura_anterior == dato) {//detectamos un llano
        longitud_tramo++;
        if (origen_tramo == -1 ) {//marcamos su origen
          origen_tramo = lecturas.indexOf(dato);
        }
      } else {  //no es un llano o se termina el tramo llano
        longitud_tramo = 0;
        origen_tramo = -1;
      }
      //actualizamos la longitud máxima del llano detectado
      if (longitud_tramo > longitudMax_tramo) {
        longitudMax_tramo = longitud_tramo;
        origen_tramoMax = origen_tramo;
      }
//      lectura_anterior = dato;
    }
    switch (longitudMax_tramo) {
      case -1:
      case  0: break;
      default: resultado.setOrigen(origen_tramoMax);
//                resultado.setDuracion(longitudMax_tramo);
        resultado.setLongitud(longitudMax_tramo);
    }
    return resultado;
  }
}
