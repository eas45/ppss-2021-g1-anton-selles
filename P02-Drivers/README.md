# Soluciones

Tabla de contenido:

* [Ejercicio 1](#markdown-header-ejercicio-1-drivers-para-calculatasamatricula)
    * [B) Implementación de los drivers](#markdown-header-b-implementacion-de-los-drivers)
    * [C) Ejecución de los drivers](#markdown-header-c-ejecucion-de-los-tests)
    * [D) Implementación de test parametrizado](#markdown-header-d-implementacion-de-test-parametrizado)
    * [E) Depuración de un error](#markdown-header-e-depuracion-de-un-error)
* [Ejercicio 2](#markdown-header-ejercicio-2-drivers-para-buscartramollanomaslargo)
    * [B) Implementación de drivers](#markdown-header-b-implementacion-de-drivers)
    * [C) Añadir tres nuevos drivers](#markdown-header-c-anadir-tres-nuevos-drivers)
    * [D) Ejecución de tests y depuración](#markdown-header-d-ejecucion-de-tests-y-depuracion)
    * [E) Implementación de test parametrizado](#markdown-header-e-implementacion-de-test-parametrizado)
* [Ejercicio 3](#markdown-header-ejercicio-3-seleccion-y-filtrado-de-las-ejecuciones-de-los-tests)
* [Ejercicio 4](#markdown-header-ejercicio-4-pruebas-de-excepciones-y-agrupaciones-de-aserciones)
* [Anotaciones/Correcciones](#markdown-header-anotacionescorrecciones)

## Ejercicio 1 - Drivers para `calculaTasaMatricula()`

### B) Implementación de los drivers

#### Tabla de casos de prueba

| Identificador de Casos de prueba *-Caminos-* | Dato de entrada 1 `edad` | Dato de entrada 2 `familiaNumerosa` | Dato de entrada 3 `repetidor` | Resultado esperado |
|:--------------------------------------------:|:------------------------:|:-----------------------------------:|:-----------------------------:|:------------------:|
|                      C1                      |            19            |                false                |              true             |       2000.00      |
|                      C2                      |            20            |                false                |             false             |       500.00       |
|                      C3                      |            23            |                 true                |              true             |       250.00       |
|                      C4                      |            67            |                false                |              true             |       250.00       |
|                      C5                      |            52            |                 true                |             false             |       400.00       |

#### Implementación C1

```java
@BeforeEach void inicializar() {
  matricula = new Matricula();
}
        
@Test
void C1_calculaTasaMatricula() {
  // Preparación de datos de entrada
  edad = 19;
  familiaNumerosa = false;
  repetidor = true;
  // Resultado esperado según la especificación
  resultadoEsperado = 2000.00f;
  resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor); // Se llama al SUT
  // Comparación de resultados
  assertEquals(resultadoEsperado, resultadoReal, 0.002f);
}
```

#### Implementación C2

```java
@Test
void C2_calculaTasaMatricula() {
  edad = 20;
  familiaNumerosa = false;
  repetidor = false;
  resultadoEsperado = 500.00f;
  resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

  assertEquals(resultadoEsperado, resultadoReal, 0.002f);
}
```

#### Implementación C3

```java
@Test
void C3_calculaTasaMatricula() {
  edad = 23;
  familiaNumerosa = true;
  repetidor = true;
  resultadoEsperado = 250.00f;
  resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

  assertEquals(resultadoEsperado, resultadoReal, 0.002f);
}
```

#### Implementación C4

```java
@Test
void C4_calculaTasaMatricula() {
  edad = 67;
  familiaNumerosa = false;
  repetidor = true;
  resultadoEsperado = 250.00f;
  resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

  assertEquals(resultadoEsperado, resultadoReal, 0.002f);
}
```

#### Implementación C5

```java
@Test
void C5_calculaTasaMatricula() {
  edad = 52;
  familiaNumerosa = true;
  repetidor = false;
  resultadoEsperado = 400.00f;
  resultadoReal = matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

  assertEquals(resultadoEsperado, resultadoReal, 0.002f);
}
```

> **¡OJO!**
> 
> La ejecución de la *fase* `test-compile` lanza todas las goals de las fases anteriores a esta. Al ser la fase número
> 13, se ejecutan las goals de las 12 fases previas y después la de esta última.
> 
> La ejecución del *plugin* `compiler:testCompile` únicamente ejecuta la goal de compilar los tests. Por lo que da error,
> ya que necesita que se compile antes el código fuente que se va a probar.

### C) Ejecución de los tests

Hay que ejecutar la fase `test`. Para ver de manera más gráfica el resultado, puede pulsarse la M roja del menú *Maven*
después la ejecución de la fase `test`, así podrán verse los tests fallidos y los superados.

### D) Implementación de test parametrizado

```java
static float tasa = 500.00f;

    Matricula matricula = new Matricula();

    @ParameterizedTest//(name = "TEST PARAMETRIZADO con edad {0}")
    @MethodSource("casosDePrueba")
    void calculaTasaMatricula(int edad, boolean familiaNumerosa, boolean repetidor, float resultadoEsperado) {
        assertEquals(resultadoEsperado, matricula.calculaTasaMatricula(edad, familiaNumerosa, repetidor), 0.002f,
                () -> generateFailureMessage(edad, familiaNumerosa, repetidor, resultadoEsperado));
    }

    // Generador de resultados esperados
    private static Stream<Arguments> casosDePrueba() {
      return Stream.of( // edad - familiaNumerosa - repetidor - resultadoEsperado
          Arguments.of(19, false, true, 2000.00f),
          Arguments.of(20, false, false, 500.00f),
          Arguments.of(23, true, true, 250.00f),
          Arguments.of(67, false, true, 250.00f),
          Arguments.of(52, true, false, 400.00f),
          Arguments.of(60, true, true, 400.00f)
      );
    }

    // Generador de mensaje de error
    private String generateFailureMessage(int edad, boolean familiaNumerosa, boolean repetidor, float resultadoEsperado) {

        String message = "Con " + edad + " años, ";

        if (familiaNumerosa) {
            message += "siendo ";
        } else {
            message += "sin ser ";
        }
        message += "familia numerosa y ";

        if (!repetidor) {
            message += "no ";
        }
        message += "habiendo repetido, la tasa debe ser de " + resultadoEsperado + " €";

        return message;
    }
```

La propiedad `name` de la anotación *@ParametrizaedTest* funciona cuando se ejecuta el test desde IntelliJ, ya que los
test con *surfire* no tienen implementado este campo en la anotación.

### E) Depuración de un error

El caso de prueba:

```
  edad: 60
  familiaNumerosa: true
  repetidor: true
  resultadoEsperado: 400
```

Debería de dar un error, pero está corregido en la práctica P01A.

> Se ha encontrado un error en el cálculo de la tasa para un estudiante de entre 51 y 64 años de edad, que al mismo tiempo
> es familia numerosa. Se ha solucionado cambiando los if de las líneas 11 y 13 por lo siguiente:
> 
> ```java
>   if ((edad > 50) && (edad < 65)) {
>        tasa = tasa - 100.00f;
>    } else if ((familiaNumerosa) || (edad >= 65)) {
>        tasa = tasa / 2;
>    }
> ```

- - -

## Ejercicio 2 - Drivers para `buscarTramoLlanoMasLargo()`

### B) Implementación de drivers

#### Tabla de casos de pruebas

| Identificador de Casos de prueba *-Caminos-* | Dato de entrada 1 `lecturas` | Resultado esperado `resultado` |
|:--------------------------------------------:|:----------------------------:|:------------------------------:|
|                      C1                      |         [10, 10, 10]         |  `origen` = 0, `longitud` = 2  |
|                      C2                      |             [10]             |  `origen` = 0, `longitud` = 0  |

#### Implementación C1

```java
@BeforeEach void inicializar() {
  resultadoReal = new Tramo();
  llano = new Llanos();
}
        
@Test
void C1A_buscarTramoLlanoMasLargo() {
  // Inicialización
  lecturas = new ArrayList<>(
          Arrays.asList(10, 10, 10)
  );
  resultadoEsperado = new Tramo(0, 2);
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  // Comprobación atributo a atributo
  assertAll("Test C1A",
            () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen()),
            () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud()));
}
```

#### Implementación C2

```java
@Test
void C2A_buscarTramoMasLargo() (
  lecturas = new ArrayList<>(
          Arrays.asList(10)
  );
  resultadoEsperado = new Tramo();
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  // Comprobación con redefinición del método equals()
  assertEquals(resultadoEsperado, resultadoReal);
}
```

### C) Añadir tres nuevos drivers

|     |    Datos de entrada   |          Resultado esperado          |
|:---:|:---------------------:|:------------------------------------:|
|     |   lista de lecturas   |                 Tramo                |
| C1B |          {-1}         | Tramo.origen = 0; Tramo.longitud = 0 |
| C2B |     {-1,-1,-1,-1}     | Tramo.origen = 0; Tramo.longitud = 3 |
| C3B | {120,140,-10,-10,-10} | Tramo.origen = 2; Tramo.longitud = 2 |

#### Implementación C1B

```java
@Test
void C1B_buscarTramoMasLargo() {
  lecturas = new ArrayList<>(
          Arrays.asList(-1)
  );
  resultadoEsperado = new Tramo();
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  assertEquals(resultadoEsperado, resultadoReal);
}
```

#### Implementación C2B

```java
@Test
void C2B_buscarTramoMasLargo() {
  lecturas = new ArrayList<>(
          Arrays.asList(-1, -1, -1, -1)
  );
  resultadoEsperado = new Tramo(0, 3);
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  assertEquals(resultadoEsperado, resultadoReal);
}
```

#### Implementación C3B

```java
@Test
void C3B_buscarTramoMasLargo() {
  lecturas = new ArrayList<>(
          Arrays.asList(120, 140, -10, -10, -10)
  );
  resultadoEsperado = new Tramo(2, 2);
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  assertEquals(resultadoEsperado, resultadoReal);
}
```

### D) Ejecución de tests y depuración

Con la implementación anterior, el mensaje de error es el siguiente:

```
expected: <ppss.Tramo@5d740a0f> but was: <ppss.Tramo@214b199c>

expected: <ppss.Tramo@3b2da18f> but was: <ppss.Tramo@5906ebcb>
```

No aporta apenas información sobre los campos. La siguiente implementación puede ser mejor:

```java
@Test
void C1B_buscarTramoMasLargo() {
  lecturas = new ArrayList<>(
          Arrays.asList(-1)
  );
  resultadoEsperado = new Tramo();
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  assertAll("Test C1B",
          () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen(), "Error en <origen>"),
          () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud(), "Error en <longitud>"));
}

@Test
void C2B_buscarTramoMasLargo() {
  lecturas = new ArrayList<>(
          Arrays.asList(-1, -1, -1, -1)
  );
  resultadoEsperado = new Tramo(0, 3);
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  assertAll("Test C2B",
          () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen(), "Error en <origen>"),
          () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud(), "Error en <longitud>"));
}

@Test
void C3B_buscarTramoMasLargo() {
  lecturas = new ArrayList<>(
          Arrays.asList(120, 140, -10, -10, -10)
  );
  resultadoEsperado = new Tramo(2, 2);
  resultadoReal = llano.buscarTramoLlanoMasLargo(lecturas);

  assertAll("Test C3B",
          () -> assertEquals(resultadoEsperado.getOrigen(), resultadoReal.getOrigen(), "Error en <origen>"),
          () -> assertEquals(resultadoEsperado.getLongitud(), resultadoReal.getLongitud(), "Error en <longitud>"));
}
```

Esta implementación devuelve lo siguiente:

```
Test C1B (1 failure)
 org.opentest4j.AssertionFailedError: Error en <longitud> ==> expected: <0> but was: <1>

Test C2B (1 failure)
 org.opentest4j.AssertionFailedError: Error en <longitud> ==> expected: <3> but was: <4>
```

Ahora sí que se puede conocer mejor el error.

El código se soluciona haciendo que la variable <lectura_anterior> se inicialice con el primer valor del array de lecturas.
Esto es posible de hacer sin problema porque en la especificación se dice que <lecturas> no será *null*.

La implementación quedaría de la siguiente forma:

```java
public Tramo buscarTramoLlanoMasLargo(ArrayList<Integer> lecturas) {
    // Comienza por el primer dato (se sabe que el array no será null)
    int lectura_anterior = lecturas.get(0);
    int longitud_tramo = 0, longitudMax_tramo = 0;
    int origen_tramo = -1, origen_tramoMax = -1;
    Tramo resultado = new Tramo();  //el origen y la longitud es CERO
    // Se inicializa la variable auxiliar
    Integer dato = null;

//        for(Integer dato:lecturas) {
    for (int i = 1; i < lecturas.size(); i++) {
        // Se recupera el dato de la posición i
        dato = lecturas.get(i);
        if (lectura_anterior == dato) {//detectamos un llano
            longitud_tramo++;
            if (origen_tramo == -1 ) {//marcamos su origen
                origen_tramo = lecturas.indexOf(dato);
            }
        } else {  //no es un llano o se termina el tramo llano
            longitud_tramo = 0;
            origen_tramo = -1;
        }
        //actualizamos la longitud máxima del llano detectado
        if (longitud_tramo > longitudMax_tramo) {
            longitudMax_tramo = longitud_tramo;
            origen_tramoMax = origen_tramo;
        }
        lectura_anterior = dato;
    }
    switch (longitudMax_tramo) {
        case -1:
        case  0: break;
        default: resultado.setOrigen(origen_tramoMax);
//                resultado.setDuracion(longitudMax_tramo);
            resultado.setLongitud(longitudMax_tramo);
    }
    return resultado;
}
```

### E) Implementación de test parametrizado

```java
Llanos resultadoReal = new Llanos();

@ParameterizedTest
@MethodSource("casosDePrueba")
void buscarTramoLlanoMasLargo(ArrayList<Integer> lecturas, Tramo resultadoEsperado) {
  assertAll("Test parametrizado con " + lecturas,
          () -> assertEquals(resultadoEsperado.getOrigen(),
                            resultadoReal.buscarTramoLlanoMasLargo(lecturas).getOrigen(),
                  () -> generateFailureMessage_Origen(lecturas, resultadoEsperado)),
          () -> assertEquals(resultadoEsperado.getLongitud(),
                            resultadoReal.buscarTramoLlanoMasLargo(lecturas).getLongitud(),
                  () -> generateFailureMessage_Longitud(lecturas, resultadoEsperado)));
}

private static Stream<Arguments> casosDePrueba() {
  return Stream.of(
          Arguments.of(new ArrayList<>(Arrays.asList(10, 10, 10)), new Tramo(0, 2)),
          Arguments.of(new ArrayList<>(Arrays.asList(10)), new Tramo()),
          Arguments.of(new ArrayList<>(Arrays.asList(-1)), new Tramo()),
          Arguments.of(new ArrayList<>(Arrays.asList(-1, -1, -1, -1)), new Tramo(0, 3)),
          Arguments.of(new ArrayList<>(Arrays.asList(120, 140, -10, -10, -10)), new Tramo(2, 2))
  );
}

// Generador de mensaje de error de ORIGEN
private String generateFailureMessage_Origen(ArrayList<Integer> lecturas, Tramo resultadoEsperado) {

  return "En las lecturas " + lecturas + " el origen está en el km " + resultadoEsperado.getOrigen();
}

// Generador de mensaje de error de LONGITUD
private String generateFailureMessage_Longitud(ArrayList<Integer> lecturas, Tramo resultadoEsperado) {

  return "En las lecturas " + lecturas + " la longitud de la llanura es de " + resultadoEsperado.getLongitud() + " km";
}
```

- - -

## Ejercicio 3 - Selección y filtrado de las ejecuciones de los tests

*Está en el fichero de configuración, en la carpeta 'resources'*.

- - -

## Ejercicio 4 - Pruebas de excepciones y agrupaciones de aserciones

|        |          Entradas          |                       |                      Resultado esperado                     |
|:------:|:--------------------------:|:---------------------:|:-----------------------------------------------------------:|
| **ID** |   **colección, numElem**   | **Elemento a borrar** | **(colección + numElem) o excepción de tipo DataException** |
|   F1   |        [1,3,5,7], 4        |           5           |                          [1,3,7], 3                         |
|   F2   |       [1,3,3,5,7], 5       |           3           |                         [1,3,5,7], 4                        |
|   F3   | [1,2,3,4,5,6,7,8,9,10], 10 |           4           |                   [1,2,3,5,6,7,8,9,10], 9                   |
|   F4   |            [], 0           |           8           |                      DataException(m1)                      |
|   F5   |        [1,3,5,7], 4        |           -5          |                      DataException(m2)                      |
|   F6   |            [], 0           |           0           |                      DataException(m3)                      |
|   F7   |        [1,3,5,7], 4        |           8           |                      DataException(m4)                      |

> - m1: “No hay elementos en la colección”
> - m2: “El valor a borrar debe ser > cero”
> - m3:”Colección vacía. Y el valor a borrar debe ser > cero”
> - m4: “Elemento no encontrado”

#### Implementación F1

```java
@Test
void F1_delete() {
  int[] coleccionEsperada = {1, 3, 7};
  int[] coleccion = {1, 3, 5, 7};
  elementoABorrar = 5;
  resultadoEsperado = new DataArray(coleccionEsperada);
  resultadoReal = new DataArray(coleccion);

  // Si salta la excepción, no debe comparar el resultado
  assertDoesNotThrow(() -> resultadoReal.delete(elementoABorrar));
  assertAll("Test F1",
          () -> assertArrayEquals(resultadoEsperado.getColeccion(), resultadoReal.getColeccion()),
          () -> assertEquals(resultadoEsperado.size(), resultadoReal.size()));
}
```

#### Implementación F2

```java
@Test
void F2_delete() {
  int[] coleccionEsperada = {1, 3, 5, 7};
  int[] coleccion = {1, 3, 3, 5, 7};
  elementoABorrar = 3;
  resultadoEsperado = new DataArray(coleccionEsperada);
  resultadoReal = new DataArray(coleccion);

  assertDoesNotThrow(() -> resultadoReal.delete(elementoABorrar));
  assertAll("Test F2",
          () -> assertArrayEquals(resultadoEsperado.getColeccion(), resultadoReal.getColeccion()),
          () -> assertEquals(resultadoEsperado.size(), resultadoReal.size()));
}
```

#### Implementación F3

```java
@Test
void F3_delete() {
  int[] coleccionEsperada = {1, 2, 3, 5, 6, 7, 8, 9, 10};
  int[] coleccion = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  elementoABorrar = 4;
  resultadoEsperado = new DataArray(coleccionEsperada);
  resultadoReal = new DataArray(coleccion);

  assertDoesNotThrow(() -> resultadoReal.delete(elementoABorrar));
  assertAll("Test F3",
          () -> assertArrayEquals(resultadoEsperado.getColeccion(), resultadoReal.getColeccion()),
          () -> assertEquals(resultadoEsperado.size(), resultadoReal.size()));
}
```

#### Implementación F4

```java
@Test
void F4_delete() {
  // Es necesario inicializarlo aunque esté vacío
  int[] coleccion = {};
  elementoABorrar = 8;
  resultadoReal = new DataArray(coleccion);

  Exception exception = assertThrows(DataException.class,
          () -> resultadoReal.delete(elementoABorrar));
  assertEquals("No hay elementos en la colección", exception.getMessage());
}
```

#### Implementación F5

```java
@Test
void F5_delete() {
  int[] coleccion = {1, 3, 5, 7};
  elementoABorrar = -5;
  resultadoReal = new DataArray(coleccion);

  Exception exception = assertThrows(DataException.class,
          () -> resultadoReal.delete(elementoABorrar));
  assertEquals("El valor a borrar debe ser > cero", exception.getMessage());
}
```

#### Implementación F6

```java
@Test
void F6_delete() {
  int[] coleccion = {};
  elementoABorrar = 0;
  resultadoReal = new DataArray(coleccion);

  Exception exception = assertThrows(DataException.class,
          () -> resultadoReal.delete(elementoABorrar));
  assertEquals("Colección vacía. Y el valor a borrar debe ser > cero", exception.getMessage());
}
```

Con la ejecución de este test se obtiene el siguiente error:

```
expected: <Colección vacía. Y el valor a borrar debe ser > cero> but was: <Colección vacía. Y el valor a borrar debe ser > 0>
```

Solamente es un error en la escritura del mensaje de la excepción que se lanza en la línea 36 de la clase *DataArray*.
Así quedarían las líneas 35 y 36:

```java
throw new DataException("Colección vacía. "+
              "Y el valor a borrar debe ser > cero");
```

#### Implementación F7

```java
@Test
void F7_delete() {
  int[] coleccion = {1, 3, 5, 7};
  elementoABorrar = 8;
  resultadoReal = new DataArray(coleccion);

  Exception exception = assertThrows(DataException.class,
          () -> resultadoReal.delete(elementoABorrar));
  assertEquals("Elemento no encontrado", exception.getMessage());
}
```

# Anotaciones/Correcciones

- No se debe usar `new` en la declaración de los objetos de los *tests no parametrizados*, para eso existe la anotación
  `@BeforeEach`.
  
- Utilizar resultados finales, no operar ni llamar a funciones para calcular el resultado esperado.

| Hecho |                       Corrección                       |
|:-----:|:------------------------------------------------------:|
| **X** | Añadir anotación @BeforeAll en los tests que se pueda. |
| **X** |  Corregir uso de operaciones para el resultado final.  |