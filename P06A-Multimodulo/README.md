# Soluciones

[imagen_any]: images/screenAny.jpg "Selección ciudad"
[imagen_paris]: images/screen.jpg "Hoteles París"
[imagen_londres]: images/screenLondon.jpg "Hoteles Londres"

## Ejercicio 1

### Dependencia en el módulo hotelWebApp

Para solucionar la dependencia del módulo ***hotelWebApp*** se debe ejecutar la **fase install** de maven en el módulo
***hotelDatabase***.

Después se ha configurado el servidor de aplicaciones **WildFly** en el fichero `pom.xml` del módulo ***hotelWebApp***.

### Despliegue de la aplicación web

#### mvn clean install

Se ejecutan desde el módulo ***hotel***.

- mvn clean

```textmate
/usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java -Dmaven.multiModuleProjectDirectory=/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel -Dmaven.home=/usr/local/apache-maven-3.6.3 -Dclassworlds.conf=/usr/local/apache-maven-3.6.3/bin/m2.conf -Dmaven.ext.class.path=/snap/intellij-idea-ultimate/289/plugins/maven/lib/maven-event-listener.jar -javaagent:/snap/intellij-idea-ultimate/289/lib/idea_rt.jar=43245:/snap/intellij-idea-ultimate/289/bin -Dfile.encoding=UTF-8 -classpath /usr/local/apache-maven-3.6.3/boot/plexus-classworlds.license:/usr/local/apache-maven-3.6.3/boot/plexus-classworlds-2.6.0.jar org.codehaus.classworlds.Launcher -Didea.version=2021.1 clean
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO] 
[INFO] hotel                                                              [pom]
[INFO] hotelDatabase                                                      [jar]
[INFO] hotelWebApp                                                        [war]
[INFO] 
[INFO] -----------------------------< ppss:hotel >-----------------------------
[INFO] Building hotel 1.0-SNAPSHOT                                        [1/3]
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ hotel ---
[INFO] 
[INFO] -------------------------< ppss:hotelDatabase >-------------------------
[INFO] Building hotelDatabase 1.0-SNAPSHOT                                [2/3]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ hotelDatabase ---
[INFO] Deleting /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelDatabase/target
[INFO] 
[INFO] --------------------------< ppss:hotelWebApp >--------------------------
[INFO] Building hotelWebApp 1.0-SNAPSHOT                                  [3/3]
[INFO] --------------------------------[ war ]---------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ hotelWebApp ---
[INFO] Deleting /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/target
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary for hotel 1.0-SNAPSHOT:
[INFO] 
[INFO] hotel .............................................. SUCCESS [  0.171 s]
[INFO] hotelDatabase ...................................... SUCCESS [  0.008 s]
[INFO] hotelWebApp ........................................ SUCCESS [  0.007 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  0.288 s
[INFO] Finished at: 2021-04-12T20:49:49+02:00
[INFO] ------------------------------------------------------------------------

Process finished with exit code 0
```

- mvn install

```textmate
/usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java -Dmaven.multiModuleProjectDirectory=/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel -Dmaven.home=/usr/local/apache-maven-3.6.3 -Dclassworlds.conf=/usr/local/apache-maven-3.6.3/bin/m2.conf -Dmaven.ext.class.path=/snap/intellij-idea-ultimate/289/plugins/maven/lib/maven-event-listener.jar -javaagent:/snap/intellij-idea-ultimate/289/lib/idea_rt.jar=35405:/snap/intellij-idea-ultimate/289/bin -Dfile.encoding=UTF-8 -classpath /usr/local/apache-maven-3.6.3/boot/plexus-classworlds.license:/usr/local/apache-maven-3.6.3/boot/plexus-classworlds-2.6.0.jar org.codehaus.classworlds.Launcher -Didea.version=2021.1 install
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO] 
[INFO] hotel                                                              [pom]
[INFO] hotelDatabase                                                      [jar]
[INFO] hotelWebApp                                                        [war]
[INFO] 
[INFO] -----------------------------< ppss:hotel >-----------------------------
[INFO] Building hotel 1.0-SNAPSHOT                                        [1/3]
[INFO] --------------------------------[ pom ]---------------------------------
[INFO] 
[INFO] --- maven-install-plugin:2.4:install (default-install) @ hotel ---
[INFO] Installing /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/pom.xml to /home/ppss/.m2/repository/ppss/hotel/1.0-SNAPSHOT/hotel-1.0-SNAPSHOT.pom
[INFO] 
[INFO] -------------------------< ppss:hotelDatabase >-------------------------
[INFO] Building hotelDatabase 1.0-SNAPSHOT                                [2/3]
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ hotelDatabase ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 0 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ hotelDatabase ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 2 source files to /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelDatabase/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ hotelDatabase ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelDatabase/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ hotelDatabase ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.22.2:test (default-test) @ hotelDatabase ---
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ hotelDatabase ---
[INFO] Building jar: /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelDatabase/target/hotelDatabase-1.0-SNAPSHOT.jar
[INFO] 
[INFO] --- maven-install-plugin:2.4:install (default-install) @ hotelDatabase ---
[INFO] Installing /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelDatabase/target/hotelDatabase-1.0-SNAPSHOT.jar to /home/ppss/.m2/repository/ppss/hotelDatabase/1.0-SNAPSHOT/hotelDatabase-1.0-SNAPSHOT.jar
[INFO] Installing /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelDatabase/pom.xml to /home/ppss/.m2/repository/ppss/hotelDatabase/1.0-SNAPSHOT/hotelDatabase-1.0-SNAPSHOT.pom
[INFO] 
[INFO] --------------------------< ppss:hotelWebApp >--------------------------
[INFO] Building hotelWebApp 1.0-SNAPSHOT                                  [3/3]
[INFO] --------------------------------[ war ]---------------------------------
[INFO] 
[INFO] --- maven-dependency-plugin:2.6:copy (default) @ hotelWebApp ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ hotelWebApp ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ hotelWebApp ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ hotelWebApp ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ hotelWebApp ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ hotelWebApp ---
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-war-plugin:2.3:war (default-war) @ hotelWebApp ---
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by com.thoughtworks.xstream.converters.collections.TreeMapConverter (file:/home/ppss/.m2/repository/com/thoughtworks/xstream/xstream/1.4.3/xstream-1.4.3.jar) to field java.util.TreeMap.comparator
WARNING: Please consider reporting this to the maintainers of com.thoughtworks.xstream.converters.collections.TreeMapConverter
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
[INFO] Packaging webapp
[INFO] Assembling webapp [hotelWebApp] in [/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/target/hotelWebApp]
[INFO] Processing war project
[INFO] Copying webapp resources [/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/src/main/webapp]
[INFO] Webapp assembled in [24 msecs]
[INFO] Building war: /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/target/hotelWebApp.war
[INFO] 
[INFO] --- maven-install-plugin:2.4:install (default-install) @ hotelWebApp ---
[INFO] Installing /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/target/hotelWebApp.war to /home/ppss/.m2/repository/ppss/hotelWebApp/1.0-SNAPSHOT/hotelWebApp-1.0-SNAPSHOT.war
[INFO] Installing /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/pom.xml to /home/ppss/.m2/repository/ppss/hotelWebApp/1.0-SNAPSHOT/hotelWebApp-1.0-SNAPSHOT.pom
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary for hotel 1.0-SNAPSHOT:
[INFO] 
[INFO] hotel .............................................. SUCCESS [  0.309 s]
[INFO] hotelDatabase ...................................... SUCCESS [  1.576 s]
[INFO] hotelWebApp ........................................ SUCCESS [  1.135 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.149 s
[INFO] Finished at: 2021-04-12T20:50:30+02:00
[INFO] ------------------------------------------------------------------------

Process finished with exit code 0
```

### mvn wildfly:start

Se arranca el servidor de aplicaciones desde hotelWebApp.

```textmate
/usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java -Dmaven.multiModuleProjectDirectory=/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp -Dmaven.home=/usr/local/apache-maven-3.6.3 -Dclassworlds.conf=/usr/local/apache-maven-3.6.3/bin/m2.conf -Dmaven.ext.class.path=/snap/intellij-idea-ultimate/289/plugins/maven/lib/maven-event-listener.jar -javaagent:/snap/intellij-idea-ultimate/289/lib/idea_rt.jar=40759:/snap/intellij-idea-ultimate/289/bin -Dfile.encoding=UTF-8 -classpath /usr/local/apache-maven-3.6.3/boot/plexus-classworlds.license:/usr/local/apache-maven-3.6.3/boot/plexus-classworlds-2.6.0.jar org.codehaus.classworlds.Launcher -Didea.version=2021.1 org.wildfly.plugins:wildfly-maven-plugin:2.0.2.Final:start
[INFO] Scanning for projects...
[INFO] 
[INFO] --------------------------< ppss:hotelWebApp >--------------------------
[INFO] Building hotelWebApp 1.0-SNAPSHOT
[INFO] --------------------------------[ war ]---------------------------------
[INFO] 
[INFO] --- wildfly-maven-plugin:2.0.2.Final:start (default-cli) @ hotelWebApp ---
[INFO] JBoss Threads version 2.3.2.Final
[INFO] JBoss Remoting version 5.0.8.Final
[INFO] XNIO version 3.6.5.Final
[INFO] XNIO NIO Implementation Version 3.6.5.Final
[INFO] ELY00001: WildFly Elytron version 1.6.0.Final
[INFO] JAVA_HOME : /usr/lib/jvm/java-11-openjdk-amd64
[INFO] JBOSS_HOME: /home/ppss/wildfly-21.0.1.Final
[INFO] JAVA_OPTS : -Xms64m -Xmx512m -Djava.net.preferIPv4Stack=true -Djava.awt.headless=true -Djboss.modules.system.pkgs=org.jboss.byteman --add-exports=java.base/sun.nio.ch=ALL-UNNAMED --add-exports=jdk.unsupported/sun.reflect=ALL-UNNAMED --add-exports=jdk.unsupported/sun.misc=ALL-UNNAMED --add-modules=java.se
[INFO] STANDALONE server is starting up.
20:51:20,141 INFO  [org.jboss.modules] (main) JBoss Modules version 1.10.2.Final
20:51:20,749 INFO  [org.jboss.msc] (main) JBoss MSC version 1.4.12.Final
20:51:20,757 INFO  [org.jboss.threads] (main) JBoss Threads version 2.4.0.Final
20:51:20,865 INFO  [org.jboss.as] (MSC service thread 1-2) WFLYSRV0049: WildFly Full 21.0.1.Final (WildFly Core 13.0.3.Final) starting
20:51:21,728 INFO  [org.wildfly.security] (ServerService Thread Pool -- 28) ELY00001: WildFly Elytron version 1.13.1.Final
20:51:22,345 INFO  [org.jboss.as.controller.management-deprecated] (Controller Boot Thread) WFLYCTL0028: Attribute 'security-realm' in the resource at address '/core-service=management/management-interface=http-interface' is deprecated, and may be removed in a future version. See the attribute description in the output of the read-resource-description operation to learn more about the deprecation.
20:51:22,375 INFO  [org.jboss.as.controller.management-deprecated] (ServerService Thread Pool -- 5) WFLYCTL0028: Attribute 'security-realm' in the resource at address '/subsystem=undertow/server=default-server/https-listener=https' is deprecated, and may be removed in a future version. See the attribute description in the output of the read-resource-description operation to learn more about the deprecation.
20:51:22,488 INFO  [org.jboss.as.server] (Controller Boot Thread) WFLYSRV0039: Creating http management service using socket-binding (management-http)
20:51:22,505 INFO  [org.xnio] (MSC service thread 1-8) XNIO version 3.8.2.Final
20:51:22,510 INFO  [org.xnio.nio] (MSC service thread 1-8) XNIO NIO Implementation Version 3.8.2.Final
20:51:22,558 WARN  [org.jboss.as.txn] (ServerService Thread Pool -- 74) WFLYTX0013: The node-identifier attribute on the /subsystem=transactions is set to the default value. This is a danger for environments running multiple servers. Please make sure the attribute value is unique.
20:51:22,561 INFO  [org.jboss.as.security] (ServerService Thread Pool -- 72) WFLYSEC0002: Activating Security Subsystem
20:51:22,564 INFO  [org.jboss.as.security] (MSC service thread 1-4) WFLYSEC0001: Current PicketBox version=5.0.3.Final-redhat-00006
20:51:22,567 INFO  [org.wildfly.extension.microprofile.jwt.smallrye._private] (ServerService Thread Pool -- 63) WFLYJWT0001: Activating WildFly MicroProfile JWT Subsystem
20:51:22,568 INFO  [org.jboss.as.naming] (ServerService Thread Pool -- 66) WFLYNAM0001: Activating Naming Subsystem
20:51:22,575 INFO  [org.jboss.as.clustering.infinispan] (ServerService Thread Pool -- 52) WFLYCLINF0001: Activating Infinispan subsystem.
20:51:22,580 INFO  [org.wildfly.extension.microprofile.metrics.smallrye] (ServerService Thread Pool -- 64) WFLYMETRICS0001: Activating Eclipse MicroProfile Metrics Subsystem
20:51:22,582 INFO  [org.wildfly.extension.microprofile.health.smallrye] (ServerService Thread Pool -- 62) WFLYHEALTH0001: Activating Eclipse MicroProfile Health Subsystem
20:51:22,585 INFO  [org.wildfly.extension.microprofile.config.smallrye._private] (ServerService Thread Pool -- 61) WFLYCONF0001: Activating WildFly MicroProfile Config Subsystem
20:51:22,575 INFO  [org.jboss.as.jsf] (ServerService Thread Pool -- 59) WFLYJSF0007: Activated the following JSF Implementations: [main]
20:51:22,613 INFO  [org.jboss.as.webservices] (ServerService Thread Pool -- 76) WFLYWS0002: Activating WebServices Extension
20:51:22,615 INFO  [org.jboss.remoting] (MSC service thread 1-1) JBoss Remoting version 5.0.19.Final
20:51:22,627 INFO  [org.wildfly.extension.microprofile.opentracing] (ServerService Thread Pool -- 65) WFLYTRACEXT0001: Activating MicroProfile OpenTracing Subsystem
20:51:22,623 INFO  [org.wildfly.extension.io] (ServerService Thread Pool -- 53) WFLYIO001: Worker 'default' has auto-configured to 12 IO threads with 96 max task threads based on your 6 available processors
20:51:22,631 INFO  [org.jboss.as.jaxrs] (ServerService Thread Pool -- 54) WFLYRS0016: RESTEasy version 3.13.2.Final
20:51:22,640 INFO  [org.jboss.as.naming] (MSC service thread 1-5) WFLYNAM0003: Starting Naming Service
20:51:22,674 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-3) WFLYUT0003: Undertow 2.2.2.Final starting
20:51:22,670 INFO  [org.jboss.as.connector] (MSC service thread 1-2) WFLYJCA0009: Starting JCA Subsystem (WildFly/IronJacamar 1.4.23.Final)
20:51:22,676 INFO  [org.jboss.as.mail.extension] (MSC service thread 1-3) WFLYMAIL0001: Bound mail session [java:jboss/mail/Default]
20:51:22,716 INFO  [org.jboss.as.connector.subsystems.datasources] (ServerService Thread Pool -- 44) WFLYJCA0004: Deploying JDBC-compliant driver class org.h2.Driver (version 1.4)
20:51:22,768 INFO  [org.jboss.as.connector.deployers.jdbc] (MSC service thread 1-3) WFLYJCA0018: Started Driver service with driver-name = h2
20:51:22,949 INFO  [org.jboss.as.ejb3] (MSC service thread 1-3) WFLYEJB0482: Strict pool mdb-strict-max-pool is using a max instance size of 24 (per class), which is derived from the number of CPUs on this host.
20:51:22,949 INFO  [org.jboss.as.ejb3] (MSC service thread 1-5) WFLYEJB0481: Strict pool slsb-strict-max-pool is using a max instance size of 96 (per class), which is derived from thread worker pool sizing.
20:51:23,004 INFO  [org.wildfly.extension.undertow] (ServerService Thread Pool -- 75) WFLYUT0014: Creating file handler for path '/home/ppss/wildfly-21.0.1.Final/welcome-content' with options [directory-listing: 'false', follow-symlink: 'false', case-sensitive: 'true', safe-symlink-paths: '[]']
20:51:23,051 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-7) WFLYUT0012: Started server default-server.
20:51:23,053 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-3) WFLYUT0018: Host default-host starting
20:51:23,190 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-7) WFLYUT0006: Undertow HTTP listener default listening on 127.0.0.1:8080
20:51:23,256 INFO  [org.jboss.as.ejb3] (MSC service thread 1-8) WFLYEJB0493: EJB subsystem suspension complete
20:51:23,349 INFO  [org.jboss.as.connector.subsystems.datasources] (MSC service thread 1-3) WFLYJCA0001: Bound data source [java:jboss/datasources/ExampleDS]
20:51:23,421 INFO  [org.jboss.as.patching] (MSC service thread 1-3) WFLYPAT0050: WildFly Full cumulative patch ID is: base, one-off patches include: none
20:51:23,428 WARN  [org.jboss.as.domain.management.security] (MSC service thread 1-8) WFLYDM0111: Keystore /home/ppss/wildfly-21.0.1.Final/standalone/configuration/application.keystore not found, it will be auto generated on first use with a self signed certificate for host localhost
20:51:23,440 INFO  [org.jboss.as.server.deployment.scanner] (MSC service thread 1-4) WFLYDS0013: Started FileSystemDeploymentService for directory /home/ppss/wildfly-21.0.1.Final/standalone/deployments
20:51:23,454 INFO  [org.jboss.as.server.deployment] (MSC service thread 1-8) WFLYSRV0027: Starting deployment of "hotelWebApp.war" (runtime-name: "hotelWebApp.war")
20:51:23,542 INFO  [org.wildfly.extension.undertow] (MSC service thread 1-2) WFLYUT0006: Undertow HTTPS listener https listening on 127.0.0.1:8443
20:51:23,609 INFO  [org.jboss.ws.common.management] (MSC service thread 1-3) JBWS022052: Starting JBossWS 5.4.2.Final (Apache CXF 3.3.7) 
20:51:24,564 INFO  [org.infinispan.CONTAINER] (ServerService Thread Pool -- 78) ISPN000128: Infinispan version: Infinispan 'Corona Extra' 11.0.4.Final
20:51:24,617 INFO  [org.infinispan.CONFIG] (MSC service thread 1-1) ISPN000152: Passivation configured without an eviction policy being selected. Only manually evicted entities will be passivated.
20:51:24,620 INFO  [org.infinispan.CONFIG] (MSC service thread 1-1) ISPN000152: Passivation configured without an eviction policy being selected. Only manually evicted entities will be passivated.
20:51:24,702 INFO  [org.infinispan.PERSISTENCE] (ServerService Thread Pool -- 78) ISPN000556: Starting user marshaller 'org.wildfly.clustering.infinispan.spi.marshalling.InfinispanProtoStreamMarshaller'
20:51:24,898 INFO  [org.jboss.as.clustering.infinispan] (ServerService Thread Pool -- 78) WFLYCLINF0002: Started http-remoting-connector cache from ejb container
20:51:25,026 INFO  [org.wildfly.extension.undertow] (ServerService Thread Pool -- 78) WFLYUT0021: Registered web context: '/hotelWebApp' for server 'default-server'
20:51:25,127 INFO  [org.jboss.as.server] (Controller Boot Thread) WFLYSRV0010: Deployed "hotelWebApp.war" (runtime-name : "hotelWebApp.war")
20:51:25,177 INFO  [org.jboss.as.server] (Controller Boot Thread) WFLYSRV0212: Resuming server
20:51:25,183 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0025: WildFly Full 21.0.1.Final (WildFly Core 13.0.3.Final) started in 5488ms - Started 418 of 643 services (375 services are lazy, passive or on-demand)
20:51:25,186 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0060: Http management interface listening on http://127.0.0.1:9990/management
20:51:25,186 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0051: Admin console listening on http://127.0.0.1:9990
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  7.206 s
[INFO] Finished at: 2021-04-12T20:51:25+02:00
[INFO] ------------------------------------------------------------------------
```

### mvn wildfly:deploy

Se depliega el *war* generado en el servidor de aplicaciones.

```textmate
/usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java -Dmaven.multiModuleProjectDirectory=/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp -Dmaven.home=/usr/local/apache-maven-3.6.3 -Dclassworlds.conf=/usr/local/apache-maven-3.6.3/bin/m2.conf -Dmaven.ext.class.path=/snap/intellij-idea-ultimate/289/plugins/maven/lib/maven-event-listener.jar -javaagent:/snap/intellij-idea-ultimate/289/lib/idea_rt.jar=42065:/snap/intellij-idea-ultimate/289/bin -Dfile.encoding=UTF-8 -classpath /usr/local/apache-maven-3.6.3/boot/plexus-classworlds.license:/usr/local/apache-maven-3.6.3/boot/plexus-classworlds-2.6.0.jar org.codehaus.classworlds.Launcher -Didea.version=2021.1 org.wildfly.plugins:wildfly-maven-plugin:2.0.2.Final:deploy
[INFO] Scanning for projects...
[INFO] 
[INFO] --------------------------< ppss:hotelWebApp >--------------------------
[INFO] Building hotelWebApp 1.0-SNAPSHOT
[INFO] --------------------------------[ war ]---------------------------------
[INFO] 
[INFO] >>> wildfly-maven-plugin:2.0.2.Final:deploy (default-cli) > package @ hotelWebApp >>>
[INFO] 
[INFO] --- maven-dependency-plugin:2.6:copy (default) @ hotelWebApp ---
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ hotelWebApp ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ hotelWebApp ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ hotelWebApp ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ hotelWebApp ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ hotelWebApp ---
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-war-plugin:2.3:war (default-war) @ hotelWebApp ---
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by com.thoughtworks.xstream.converters.collections.TreeMapConverter (file:/home/ppss/.m2/repository/com/thoughtworks/xstream/xstream/1.4.3/xstream-1.4.3.jar) to field java.util.TreeMap.comparator
WARNING: Please consider reporting this to the maintainers of com.thoughtworks.xstream.converters.collections.TreeMapConverter
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
[INFO] Packaging webapp
[INFO] Assembling webapp [hotelWebApp] in [/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/target/hotelWebApp]
[INFO] Processing war project
[INFO] Copying webapp resources [/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/src/main/webapp]
[INFO] Webapp assembled in [42 msecs]
[INFO] Building war: /home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp/target/hotelWebApp.war
[INFO] 
[INFO] <<< wildfly-maven-plugin:2.0.2.Final:deploy (default-cli) < package @ hotelWebApp <<<
[INFO] 
[INFO] 
[INFO] --- wildfly-maven-plugin:2.0.2.Final:deploy (default-cli) @ hotelWebApp ---
[INFO] JBoss Threads version 2.3.2.Final
[INFO] JBoss Remoting version 5.0.8.Final
[INFO] XNIO version 3.6.5.Final
[INFO] XNIO NIO Implementation Version 3.6.5.Final
[INFO] ELY00001: WildFly Elytron version 1.6.0.Final
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.252 s
[INFO] Finished at: 2021-04-12T20:52:40+02:00
[INFO] ------------------------------------------------------------------------

Process finished with exit code 0
```

### http://localhost:8080/HotelWebApp

Se ejecuta la aplicación web desde el navegador.

![Imagen inicial][imagen_any]

![Imagen_París][imagen_paris]

![Imangen_Londres][imagen_londres]

### mvn wildfly:shutdown

Se detiene el servidor.git

```textmate
/usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java -Dmaven.multiModuleProjectDirectory=/home/ppss/Desktop/ppss-2021-g1-anton-selles/P06A-Multimodulo/multimodulos/hotel/hotelWebApp -Dmaven.home=/usr/local/apache-maven-3.6.3 -Dclassworlds.conf=/usr/local/apache-maven-3.6.3/bin/m2.conf -Dmaven.ext.class.path=/snap/intellij-idea-ultimate/289/plugins/maven/lib/maven-event-listener.jar -javaagent:/snap/intellij-idea-ultimate/289/lib/idea_rt.jar=39001:/snap/intellij-idea-ultimate/289/bin -Dfile.encoding=UTF-8 -classpath /usr/local/apache-maven-3.6.3/boot/plexus-classworlds.license:/usr/local/apache-maven-3.6.3/boot/plexus-classworlds-2.6.0.jar org.codehaus.classworlds.Launcher -Didea.version=2021.1 org.wildfly.plugins:wildfly-maven-plugin:2.0.2.Final:shutdown
[INFO] Scanning for projects...
[INFO] 
[INFO] --------------------------< ppss:hotelWebApp >--------------------------
[INFO] Building hotelWebApp 1.0-SNAPSHOT
[INFO] --------------------------------[ war ]---------------------------------
[INFO] 
[INFO] --- wildfly-maven-plugin:2.0.2.Final:shutdown (default-cli) @ hotelWebApp ---
[INFO] JBoss Threads version 2.3.2.Final
[INFO] JBoss Remoting version 5.0.8.Final
[INFO] XNIO version 3.6.5.Final
[INFO] XNIO NIO Implementation Version 3.6.5.Final
[INFO] ELY00001: WildFly Elytron version 1.6.0.Final
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.067 s
[INFO] Finished at: 2021-04-12T20:58:11+02:00
[INFO] ------------------------------------------------------------------------

Process finished with exit code 0
```

## Ejercicio 2

Orden de ejecución de la **fase install**:

1) matriculacion-comun
    1) Se añade la dependencia a los pom de matriculacion-dao y matriculacion-proxy
2) matriculacion-dao
3) matriculacion-proxy
    1) Se añaden las dependencias al pom de matriculacion-bo
4) matriculacion-bo
