package ejercicio1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MultipathExampleTest {

  MultipathExample multipath;

  @BeforeEach
  void init() {
    multipath = new MultipathExample();
  }

  @Test
  void C1_multiPah1() {
    int resultadoEsperado = 14;
    int resultadoReal = multipath.multiPah1(6, 6, 2);

    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  void C2_multiPah1() {
    int resultadoEsperado = 2;
    int resultadoReal = multipath.multiPah1(2, 2, 2);

    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  void CasoDePrueba_Enunciado() {
    int resultadoEsperado = 8;
    int resultadoReal = multipath.multiPah1(3, 6, 2);

    assertEquals(resultadoEsperado, resultadoReal);
  }

  @ParameterizedTest
  @MethodSource("generator1")
  void multiPath2(int a, int b, int c, int resultadoEsperado) {
    assertEquals(resultadoEsperado, multipath.multiPath2(a, b, c));
  }

  private static Stream<Arguments> generator1() {
    return Stream.of(
            Arguments.of(6, 3, 7, 16),
            Arguments.of(2, 2, 5, 5),
            Arguments.of(8, 6, 7, 13)
    );
  }

  @ParameterizedTest
  @MethodSource("generator2")
  void multiPath3(int a, int b, int c, int resultadoEsperado) {
    assertEquals(resultadoEsperado, multipath.multiPath3(a, b, c));
  }

  private static Stream<Arguments> generator2() {
    return Stream.of(
            Arguments.of(6, 3, 8, 17),
            Arguments.of(2, 5, 4, 4)
    );
  }
}