# Soluciones

## Plugin Jacoco

```xml
<!-- PLUGIN JACOCO -->
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.7</version>

    <executions>
        <!-- Preparación de la instrumentación y el análisis de cobertura -->
        <execution>
            <id>default-prepare-agent</id>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <!-- Generación del informe -->
        <execution>
            <id>default-report</id>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
        <!-- Establecimiento de niveles de cobertura -->
        <execution>
            <id>default-check</id>
            <goals>
                <goal>check</goal>
            </goals>
            <configuration>
                <!-- Podemos añadir reglas -->
                <rules>
                </rules>
            </configuration>
        </execution>
    </executions>
</plugin>
```

## Ejercicio 1

### A) Complejidad ciclomática `multiPathExample()` y cobertura de 100% de líneas y condiciones

![Grafo multiPathExample][multipathexample_grafo]

#### Fórmulas del cálculo de la Complejidad Ciclomática (CC)
  - *CC = Nº Arcos - Nº Nodos + 2 =* 6 - 5 + 2 = 3
  - *CC = Nº Regiones =* 3
  - *CC = Nº Condiciones + 1 =* 2 + 1 = 3
  
#### Casos de prueba

| Identificador de Casos de prueba *-Caminos-* |     | Datos de entrada |     | Resultado esperado |
|:--------------------------------------------:|:---:|:----------------:|:---:|:------------------:|
|                                              | `a` |        `b`       | `c` |                    |
|                   ***C1***                   |  6  |         6        |  2  |         14         |
|                   ***C2***                   |  2  |         2        |  2  |          2         |

> ***Nota***
>
> El nivel 4 se trata de conseguir la cobertura de condiciones y decisiones.
>
> Con un **nivel 4** de cobertura se puede conseguir lo que se pide. Porque un 100% de **cobertura de ramas (condiciones)**
> implica un 100% de **cobertura de líneas**.
>
> Sin embargo, con un 100% de cobertura de condiciones NO se consigue un 100% de cobertura de líneas.

#### Codificación de los drivers

```java
class MultipathExampleTest {

  MultipathExample multipath;

  @BeforeEach
  void init() {
    multipath = new MultipathExample();
  }

  @Test
  void C1_multiPah1() {
    int resultadoEsperado = 14;
    int resultadoReal = multipath.multiPah1(6, 6, 2);

    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  void C2_multiPah1() {
    int resultadoEsperado = 2;
    int resultadoReal = multipath.multiPah1(2, 2, 2);

    assertEquals(resultadoEsperado, resultadoReal);
  }
}
```

### B) Informe de cobertura

No se ha configurado ninguna regla.

La ruta en la que se encuentra el archivo `index.html` que nos permite visualizar los datos de informe es:

![Ruta index.html][ruta_index]

Y el resultado del informe, a nivel de clase, es:

![Resultado ejercicio 1 B][resultados_ejercicio1_B]

### C) Intento de generación de informe y solución del problema

El caso de prueba que hay que añadir es el siguiente - `CasoDePrueba_Enunciado`:

`a = 7; b = 7; c = 7; resultadoEsperado = 7;`

Lo que sucederá es que el assert del driver dará error, porque el resultado esperado no coincide con el real *(21)* y
no se generará ningún informe de Jacoco.

> ***Solución***
> 
> Cambiar el resultado esperado por **21**, que es el valor correcto.

### D) Generación de un nuevo informe con el método `multiPath2`

Se cambia `CasoDePrueba_Enunciado` por:

`a = 3; b = 6; c = 2; resultadoEsperado = 8;`

Se añade el siguiente método a la clase *MultipathExample*:

```java
public int multiPath2(int a, int b, int c) {
  if ((a > 5) & (b < 5)) {
    b += a;
  }
  if (c > 5) {
    c += b;
  }
  return c;
}
```

#### Grafo

![Grafo Ejercicio1D][grafo_ejercicio1D]

***CC (JaCOCO) = B - D + 1***

- *B (número de "branches")* = {1-2, 1-4, 2-3, 2-4, 4-5, 4-6} = 6
- *D (número de "decision points")* = {1, 2, 4} = 3

CC = B - D + 1 = 6 - 3 + 1 = 4

#### Casos de prueba

| Identificador de Casos de prueba *-Caminos-* |     | Datos de entrada |     | Resultado esperado |
|:--------------------------------------------:|:---:|:----------------:|:---:|:------------------:|
|                                              | `a` |        `b`       | `c` |                    |
|                   ***C3***                   |  6  |         3        |  7  |         16         |
|                   ***C4***                   |  2  |         2        |  5  |          5         |
|                   ***C5***                   |  8  |         6        |  7  |         13         |

#### Codificación de los drivers

```java
@ParameterizedTest
@MethodSource("generator")
void multiPath2(int a, int b, int c, int resultadoEsperado) {
  assertEquals(resultadoEsperado, multipath.multiPath2(a, b, c));
}

private static Stream<Arguments> generator() {
  return Stream.of(
          Arguments.of(6, 3, 7, 16),
          Arguments.of(2, 2, 5, 5),
          Arguments.of(8, 6, 7, 13)
  );
}
```

#### Informe

![Resultado_Ejercicio_1_D][resultados_ejercicio1_D]

> ***Nota***
> 
> Cuando hay un **&&** en una decisión, si una condición da `false`, las condiciones que pueda haber a continuación no se
> comprueban.Por eso se necesita de tres casos de prueba para conseguir una cobertura del 100% de condiciones y decisiones.

### E) Generación de un nuevo informe con el método `multiPath3`

Se añade el siguiente método a la clase *MultipathExample*:

```java
public int multiPath2(int a, int b, int c) {
  if ((a > 5) & (b < 5)) {
    b += a;
  }
  if (c > 5) {
    c += b;
  }
  return c;
}
```

#### Grafo

![Grafo Ejercicio1E][grafo_ejercicio1E]

***CC (JaCOCO) = B - D + 1***

- *B (número de "branches")* = {1-2, 1-3, 2-4, 2-5, 3-5, 3-5, 5-6, 5-7} = 8
- *D (número de "decision points")* = {1, 2, 3, 5} = 4

CC = B - D + 1 = 8 - 4 + 1 = 5

#### Casos de prueba

| Identificador de Casos de prueba *-Caminos-* |     | Datos de entrada |     | Resultado esperado |
|:--------------------------------------------:|:---:|:----------------:|:---:|:------------------:|
|                                              | `a` |        `b`       | `c` |                    |
|                   ***C6***                   |  6  |         3        |  8  |         17         |
|                   ***C7***                   |  2  |         5        |  4  |          4         |

#### Codificación de drivers

```java
@ParameterizedTest
@MethodSource("generator2")
void multiPath3(int a, int b, int c, int resultadoEsperado) {
  assertEquals(resultadoEsperado, multipath.multiPath2(a, b, c));
}

private static Stream<Arguments> generator2() {
  return Stream.of(
          Arguments.of(6, 3, 8, 17),
          Arguments.of(2, 5, 4, 4)
  );
}
```

#### Informe

![Resultado_Ejercicio_1_E][resultados_ejercicio1_E]

## Ejercicio 2



- - -
[multipathexample_grafo]: Images/CC-multiPathExample.png "Diagrama multiPathExample"
[ruta_index]: Images/rutaIndex.jpg "Ruta index.html"
[resultados_ejercicio1_B]: Images/informeE1B.jpg "Resultado del informe del ejercicio 1 B"
[grafo_ejercicio1D]: Images/CC_Ejercicio1D.png "Grafo Ejercicio 1 D"
[resultados_ejercicio1_D]: Images/informeE1D.jpg "Resultado del informe del ejercicio 1 D"
[grafo_ejercicio1E]: Images/CC_Ejercicio1E.png "Grafo Ejercicio 1 E"
[resultados_ejercicio1_E]: Images/informeE1E.jpg "Resultado del informe del ejercicio 1 E"