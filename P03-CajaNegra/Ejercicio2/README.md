#Soluciones

## Ejercicio 2 - especificación generaTicket()

### Entradas

#### Entrada 1 (E1)

*cliente* - Su NIF debe estar registrado y no debe tener una deuda mayor a 1000€

El objeto está compuesto de `nif`, `estado` y `deuda`.

> *ANTES*
> 
> - *cliente*
>     - Clase válida:
>         - `V1`: NIF registrado y la deuda es menor o igual a 1000
>     - Clases inválidas:
>         - `N1`: NIF no registrado
>         - `N2`: deuda mayor que 1000
>         - `N3`: Referencia a null
> 
> Subclases:
> 
> - *nif* - Debe estar registrado en la base de datos
>     - Clases válidas:
>         - `V11`: NIF registrado
>     - Clases inválidas:
>         - `N11`: NIF no registrados
>         - `N`: es NULL
> 
> 
> - *estado* - Puede ser uno de los valores: "NORMAL" o "MOROSO"
>     - Clases válidas:
>         - `V2`: "NORMAL"
>         - `V3`: "MOROSO"
>   
> 
> - *deuda* - No deberá superar el valor 1000
>     - Clase válida:
>         - `V4`: Valor menor o igual que 1000 (deuda <= 1000)
>     - Clase inválida:
>         - `N3`: Valor mayor que 1000 (deuda > 1000)

- ***cliente*** - *Heurística 6: Subdivisión de la partición* - Clase compuesta por `String nif`, `EstadoCliente estado`
  y `float deuda`
    - Clases válidas:
        - `V1`: cliente registrado normal - (nif registrado, estado: normal, deuda == 0)
        - `V2`: cliente registrado moroso - (nif registrado, estado: moroso, deuda > 0 && deuda <= 1000)
    - Clases inválidas:
        - `N1`: nif no registrado - (nif no registrado, estado: normal, deuda == 0) || (nif no registrado, estado: moroso,
          deuda > 0 && deuda <= 1000)
        - `N2`: cliente == null
        - `N3`: deuda superior a 1000€ - (nif registrado, estado: moroso, deuda > 1000)

#### Entrada 2 (E2)

> *ANTES*
> 
> *codArticulos* - El código de cada artículo debe estar en la base de datos
> 
> Es una lista de artículos, cada artículo se compone de `cod` y `precioUnitario`.
> 
> - *cod* - Cualquier valor es válido (no se especifica nada)
>     - Clase válida:
>         - `V6`: El valor es válido
>   
> 
> - *precioUnitario* - Cualquier valor es válido (no se comentan restricciones)
>     - Clase válida:
>         - `V7`: Cualquier valor
>   
> 
> - *codArticulo* - Los artículos deben existir y no debe haber problemas en el acceso a la base de datos
>     - Clase válida:
>         - `V8`: Los códigos existen y no ha habido ningún problema accediendo a ella
>     - Clase inválida:
>         - `N7`: Algún código no existe en la base de datos

- ***codArticulos*** - *Heurística 3: Conjunto de valores* - Objeto `List<String>`
  - Clase válida:
    - `V3`: lista sin códigos repetidos
    - `V4`: lista con códigos repetidos
  - Clases inválidas:
    - `N4`: artículo no registrado en la base de datos
    - `N5`: lista vacía
    - `N6`: lista de artículos null (codArticulos == null)
  
#### Entrada 3 (E3)

- ***acceso a base de datos*** - *Heurística 5: Debe ser* - Se podrá acceder o no
  - Clase válida:
    - `V5`: acceso a la base de datos
  - Clase inválida:
    - `N7`: error en el acceso a la base de datos> 
  
### Salida (S)

*Ticket de compra*

> *ANTES*
> 
> - Clase válida:
>     - `S1`: Ticket
>   
> - Clase inválida:
>     - `NS1`: BOException("El cliente no puede realizar la compra")
>     - `NS2`: BOException("El artículo no está en la BD")
>     - `NS3`: BOException("Error al recuperar datos del artículo")

- Clase válida:
    - `S1`: Ticket (Cliente de entrada, lineas con artículos de entrada y precio total)
- Clases inválidas:
    - `NS1`: BOException("El cliente no puede realizar la compra") - nif no registrado || cliente == null || deuda > 1000
    - `NS2`: BOException("El artículo no está en la BD) - Artículo no registrado
    - `NS3`: BOException("Error al recuperar datos del artículo") - Error de acceso a la base de datos
    - `NS4`: ? - codArticulo == null o lista vacía
  
### Casos de prueba

#### Asunciones

- Clientes registrados en la base de datos

| Cliente |   `nif`   | `estado` |  `deuda` |      Observación      |
|:-------:|:---------:|:--------:|:--------:|:---------------------:|
|    C1   | 12345678A |  normal  |   0.00f  |   Cliente registrado  |
|    C2   | 23456789B |  moroso  |  100.00f |   Cliente registrado  |
|    C3   | 11223344C |  moroso  | 1200.00f |   Cliente registrado  |
|    C4   | 55667788D |  normal  |   0.00f  | Cliente no registrado |

- Artículos existentes en la base de datos

| Artículo | `cod` | `precioUnitario` |               Observaciones               |
|:--------:|:-----:|:----------------:|:-----------------------------------------:|
|    A1    |  C001 |      10.00f      |   Artículo existente en la base de datos  |
|    A2    |  C002 |      200.00f     |   Artículo existente en la base de datos  |

#### Tabla

|       Clases       | Dato de entrada 1 `cliente` |          |          | Dato de entrada 2 `codArticulo` | Dato de entrada 3 `Acceso BD` |  Resultado esperado `Ticket` |                                                               |               |               `BOException`              |
|:------------------:|:---------------------------:|:--------:|:--------:|:-------------------------------:|:-----------------------------:|:----------------------------:|:-------------------------------------------------------------:|:-------------:|:----------------------------------------:|
|                    |            `nif`            | `estado` |  `deuda` |                                 |                               |           `cliente`          |                            `lineas`                           | `precioTotal` |                                          |
|  ***V1-V3-V5-S1*** |          12345678A          |  normal  |   0.00f  |           {C001, C002}          |               OK              |  {12345678A; normal; 0.00f}  | {[(C001, 10.00f); 1; 10.00f];  [(C002, 200.00f); 1; 200.00f]} |    210.00f    |                                          |
|  ***V2-V4-V5-S1*** |          23456789B          |  moroso  |  100.00f |        {C001, C002, C002}       |               OK              | {23456789B; moroso; 100.00f} | {[(C001, 10.00f); 1; 10.00f];  [(C002, 200.00f); 2; 200.00f]} |    410.00f    |                                          |
| ***N1-V3-V5-NS1*** |          55667788D          |  normal  |   0.00f  |              {C001}             |               OK              |                              |                                                               |               | "El cliente no puede realizar la compra" |
| ***N2-V3-V5-NS1*** |            *null*           |  *null*  |  *null*  |              {C001}             |               OK              |                              |                                                               |               | "El cliente no puede realizar la compra" |
| ***N3-V3-V5-NS1*** |          11223344C          |  moroso  | 1200.00f |              {C002}             |               OK              |                              |                                                               |               | "El cliente no puede realizar la compra" |
| ***V1-N4-V5-NS2*** |          12345678A          |  normal  |   0.00f  |              {C003}             |               OK              |                              |                                                               |               |      "El artículo no está en la BD"      |
| ***V1-N5-V5-NS4*** |          12345678A          |  normal  |   0.00f  |                {}               |               OK              |               ?              |                               ?                               |       ?       |                     ?                    |
| ***V1-N6-V5-NS4*** |          12345678A          |  normal  |   0.00f  |              *null*             |               OK              |               ?              |                               ?                               |       ?       |                     ?                    |
| ***V1-V3-N7-NS3*** |          12345678A          |  normal  |   0.00f  |              {C001}             |             Fallo             |                              |                                                               |               |  "Error al recuperar datos del artículo" |
  
> *ANOTACIONES*
> 
> Según tengo entendido, en cada fila de la tabla se considera una entrada no válida distinta. De la manera en que yo
> lo tengo hecho, esto no es así. Pero no comprendo la forma en que debería de hacerlo. ¿Que un objeto sea una entrada
> hace que no se considere al objeto como entrada y pasen a ser sus atributos las entradas?``