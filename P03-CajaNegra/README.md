# Anotaciones solución práctica

## Ejercicio 1

### Paso 1 - ¿Cuántas entradas y salidas hay?

- **4 entradas**
    - tipo
    - fecha_inicio
    - num_dias
    - disponibilidad del coche (aunque no sea un argumento del método)
  
- **1 salida**

#### Entradas

- ***tipo*** - *Heurística 3: Conjunto de valores* - Tipo enumerado: "TURISMO", "DEPORTIVO" (No es poible que entre algo distinto)
    - Clase válida: "TURISMO" o "DEPORTIVO"
  

- ***fecha_inicio*** - *Heurística 5: Debe ser* - Partición DEBE SER
    - Clase válida: fecha_inicio > fecha_actual
      
    - Clases inválidas:
        - fecha_inicio <= fecha_actual
        - fecha == null


- *num_dias* - Es un entero (no se especifica que no pueda entrar un negativo)
    - Clase válida: 1 a 30
        - Subdivisión:
            - 1 día
            - 2 o más días
      
    - Clases inválidas:
        - mayor de 30
        - menor de 1
  

- *disponbilidad* - Es un booleano, solo puede tener valores True o False
    - Clase válida:
        - True
    - Clase inválida:
        - False


#### Salida

- Clases válidas:
    - Un día (importe = 100)
    - Dos o más días (importe = num_dias * 50)
  
- Clases inválidas:
    - "Reserva no posible"
    - "Fecha no correcta"
    - ? (num_dias < 1)
    - ? (fecha_inicio == null)
  
*(7 casos de prueba en la tabla)*

Primero se cubren todas las particiones válidas, después se cubren las inválidas, de una en una.

## Ejercicio 2

> Las respuestas están subidas a un PDF.


## Ejercicio 3

- **3 entradas**
    - alumno
    - asignaturas
    - resultado de acceder a la base de datos
  
- **1 salida**

### Entradas

1. *alumno* - Los atributos están validados previamente (no es necesario particionar por todos ellos, únicamente por NIF)
   y el objeto va a poder ser NULL
    - Clase válida
        1. nif correcto (puede ser que el alumno esté registrado o no)
        - SUBPARTICIÓN:
            1. No existe en la BD 
            2. Existe en la BD
      
    - Clases inválidas
        1. nif incorrecto
        2. nif == null
  

2. *asignaturas* - Es una lista (lista de 1 a 5 asigaturas, lista con más de 5 asignaturas)
    - Clase válida:
        1. Lista de 1 a 5 asignaturas (todos los códigos son nuevos)
    - Clases inválidas
        1. null
        2. vacía
        3. Lista de 1 a 5 asignaturas (con algún código ya matriculado)
        4. Lista con más de 5 asignaturas (con códigos nuevos)
  
3. *resultado de acceder a la base de datos*
    - Clase válida:
        1. Acceso correcto
    - Clases inválidas:
        1. Error al matricular
        2. Error en alumno
        3. Error al dar de alta
  
### Salida

- Clases válidas: (Considerado como matrículaTO es válido y si da excepción es inválido, puede cambiarse, no afecta)
    1. Alumno igual al que entra, lista de asignaturas con todas las asignaturas de entrada, lista de errores vacía
    2. Alumnno de entrada, lista de asignaturas con algunas asignaturas, lista de errores no vacía

- Clases inválidas:
    1. nif nulo
    2. dato 
    3. 
    4. 
    5. 
    6. 
    7. 
  
*(11 casos de prueba en la tabla)*

## Respuesta de dudas

Ej 1. Si no se dice nada sobre el número de días, no hay que asumir nada. En este caso, si el número de días fuese < 1 la salida es ???. Fíjate que la entrada asociada al número de días es una variable de tipo entero. A menos que te digan que es imposible que dicha variable tome esos valores, debemos considerar las particiones inválidas que nos sugieren las heurísticas.

Con respecto a si debería ser S2 o NS3, lo normal es que si asociamos esa salida con entradas válidas, la salida también lo sea, y si la asociamos con una entrada no válida, la salida entonces debería ser no válida. Ahora bien, si etiquetas una SALIDA como válida o inválida, tu tabla es la misma, con lo cual digamos que da un poco igual cómo etiquetes la salida. Con las ENTRADAS sí que importa si las etiquetas como válidas o como inválidas.

