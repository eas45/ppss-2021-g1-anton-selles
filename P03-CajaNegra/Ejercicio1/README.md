# Soluciones

## Ejercicio 1 - especificación importe_alquiler_coche()

> **Especificación**
> 
> En una aplicación de un negocio de alquiler de coches necesitamos una unidad denominada
> **importe_alquiler_coche()**. Dicha unidad calcula el importe del alquiler de un determinado tipo de
> coche durante un cierto número de días, a partir de una fecha concreta, y devuelve el importe del
> alquiler. Si no es posible realizar los cálculos devuelve una excepción de tipo ReservaException. El
> prototipo del método es el siguiente:
> 
> `public float importe_alquiler_coche (TipoCoche tipo, Date fecha_inicio, int num_dias) throws ReservaException`
> 
> TipoCoche es un tipo enumerado cuyos posibles valores son: (TURISMO, DEPORTIVO). Nos indican que
> si la fecha de inicio proporcionada no es posterior a la actual, entonces se lanzará la excepción
> ReservaException con el mensaje "Fecha no correcta". Si el tipo de coche no está disponible durante los
> días requeridos, o se intenta hacer una reserva de más de 30 días, entonces se lanzará la excepción
> ReservaException con el mensaje "Reserva no posible". El precio de la reserva por día depende del
> número de días reservados, según la siguiente tabla:
> 
> 1 día -> 100 euros
> 
> 2 días o más -> 50 euros
> 
> Diseña los casos de prueba para la especificación anterior utilizando el método de particiones
> equivalentes.

Una entrada válida es [TURISMO, DEPORTIVO] + fecha posterior a la actual + dias <= 30

> **IMPORTANTE**
> 
> Hay que tener en cuenta que también puede haber entradas que no aparezcan en los argumentos del método. Se debe
> considerar si es una entrada, viendo si afecta al comportamiento del método.

### Entradas

#### Entrada 1 (E1)

***tipo*** - *Heurística 3: Conjunto de valores* - Tipo enumerado: TURISMO, DEPORTIVO (No es poible que entre algo distinto)

> *Antes*
> 
> - Clases válidas:
>     - `V1`: "TURISMO"
>     - `V2`: "DEPORTIVO"
> 
> No es necesario particionar ambos valores del conjunto, ya que no afectan directamente a ninguna funcionalidad.

- Clase válida:
    - `V1`: TURISMO o DEPORTIVO
  
#### Entrada 2 (E2)

***fecha_inicio*** - *Heurística 5: Debe ser* - Debe ser posterior a la actual

> *Antes*
> 
> - Clase válida:
>     - `V3`: fecha de inicio posterior a la actual (fecha_inicio > fecha_actual)
> - Clase inválida:
>     - `N1`: fecha de inicio anterior o igual a actual (fecha_inicio <= fecha_actual)
> 
> Al ser un objeto Date y no haber sido validado, se debe tener en cuenta que podría ser NULL

- Clase válida:
    - `V2`: fecha de inicio posterior a la actual (fecha_inicio > fecha_actual)
  
- Clase inválida:
    - `N1`: fecha de inicio igual o inferior a la actual (fecha_inicio <= fecha_actual)
    - `N2`: fecha es NULL (fecha_inicio == null)

#### Entrada 3 (E3)

***num_dias*** - *Heurística 1: Rango* - Es un entero (no se especifica que no pueda entrar un negativo)

- Clase válida:
    - `V3`: valor mayor o igual a 1 y menor o igual a 30 (num_dias >= 1 && num_dias <= 30)
  
    *Subdivisión: El precio varía según la cantidad de días*
    - `V31`: 1 día
    - `V32`: 2 o más días
  
- Clase inválida:
    - `N3`: valor menor que 1 (num_dias < 1)
    - `N4`: valor mayor que 30 (num_dias > 30)
  
#### Entrada 4 (E4)

***disponible*** - *Heurísitca 3: Conjunto de valores* - Será un booleano

- Clase válida:
    - `V4`: true

- Clase inválida:
    - `N5`: false
  
### Salida (S)

*Importe del alquiler del coche*

> *Antes*
> 
> - Clases válidas:
>     - `S1`: Importe del alquiler del coche
>     - `S2`: El tipo de coche no está disponible durante los días requeridos - ReservaException("Reserva no posible")
>   
> - Clases inválidas:
>     - `NS1`: ReservaException("Fecha no correcta")
>     - `NS2`: ReservaException("Reserva no posible")

- Clases válidas:
    - `S1`: Alquiler de 1 día (importe = 100)
    - `S2`: Alquiler de 2 o más días (importe = num_dias * 50)
  
- Clases inválidas:
    - `NS1`: ReservaException("Fecha no correcta")
    - `NS2`: ReservaException("Reserva no posible")
    - `NS3`: ? - fecha_inicio == null
    - `NS4`: ? - num_dias < 1
  
### Casos de prueba

#### Asunciones

1. La fecha actual es 09/03/2021

#### Tabla

|      Clases      | Dato de entrada 1 `tipo` | Dato de entrada 2 `fecha_inicio` | Dato de entrada 3 `num_dias` | Dato de entrada 4 `disponible` |           Resultado esperado           |
|:----------------:|:------------------------:|:--------------------------------:|:----------------------------:|:------------------------------:|:--------------------------------------:|
|  V1-V2-V31-V4-S1 |          TURISMO         |            28/06/2021            |               1              |              true              |                 100.00f                |
|  V1-V2-V32-V4-S2 |         DEPORTIVO        |            28/06/2021            |               3              |              true              |                 150.00f                |
| V1-N1-V31-V4-NS1 |          TURISMO         |            28/06/1997            |               1              |              true              |  ReservaException("Fecha no correcta") |
| V1-N2-V32-V4-NS3 |         DEPORTIVO        |               null               |               5              |              true              |                    ?                   |
|  V1-V2-N3-V4-NS4 |          TURISMO         |            28/06/2021            |               0              |              true              |                    ?                   |
|  V1-V2-N4-V4-NS2 |         DEPORTIVO        |            28/06/2021            |              40              |              true              | ReservaException("Reserva no posible") |
| V1-V2-V32-N5-NS2 |          TURISMO         |            28/06/2021            |              15              |              false             | ReservaException("Reserva no posible") |

- - -

> ***ANOTACIÓN***
> 
> - Si no se dice nada, ¿los valores del número de días deben ser un rango de 1 a 30, por el sentido común?
> - ¿`S2` debería considerarse como `NS2`, aunque las entradas sean válidas, ya que se está lanzando una excepción?
> 
> **Respuesta**
> 
> Ej 1. Si no se dice nada sobre el número de días, no hay que asumir nada. En este caso, si el número de días fuese < 1
> la salida es ???. Fíjate que la entrada asociada al número de días es una variable de tipo entero. A menos que te digan
> que es imposible que dicha variable tome esos valores, debemos considerar las particiones inválidas que nos sugieren
> las heurísticas.
> 
> Con respecto a si debería ser S2 o NS3, lo normal es que si asociamos esa salida con entradas válidas, la salida también
> lo sea, y si la asociamos con una entrada no válida, la salida entonces debería ser no válida. Ahora bien, si etiquetas
> una SALIDA como válida o inválida, tu tabla es la misma, con lo cual digamos que da un poco igual cómo etiquetes la salida.
> Con las ENTRADAS sí que importa si las etiquetas como válidas o como inválidas.