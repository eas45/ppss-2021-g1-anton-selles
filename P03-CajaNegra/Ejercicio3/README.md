# Soluciones

## Ejercicio 3 - especificación *matriculaAlumno()*

Las entradas son:
- La información del alumno `AlumnoTO`
- El listado de asignaturas `List<AsignaturaTO>`
- El acceso a la base de datos

La salida es:
- Los datos de la matriculación `MatriculaTO`

### Entradas

#### Entrada 1 (E1)

***alumno*** - No puede ser null, pero el nif no ha sido validado

- Clases válidas:
    - `V1`: nif válido, dado de alta
    - `V2`: nif válido, no dado de alta

- Clases no válidas:
    - `N1`: nif no válido
    - `N2`: nif == null
  
#### Entrada 2 (E2)

***asignaturas*** - No se podrá matricular de una asignatura ya matriculada, el máximo de asignaturas es 5

- Clase válida:
    - `V3`: lista de asignaturas no matriculadas y nº de asignaturas > 0 y <= 5
  
- Clases inválidas:
    - `N3`: lista vacía
    - `N4`: lista nula
    - `N5`: en alguna asignatura ya se está matriculado
    - `N6`: lista con más de 5 asignaturas
  
#### Entrada 3 (E3)

***acceso a BD***

- Clase válida:
    - `V4`: OK
  
- Clase inválida:
    - `N7`: Error al obtener datos del alumno
    - `N8`: Error de alta del alumno
    - `N9`: Error al matricular de una asignatura
  
### Salida (S)

- Clase válida:
    - `S1`: alumno matriculado de las asignaturas
    - `S2`: alumno matriculado de algunas asignaturas (por fallo en la base de datos) + lista errores ("Error al matricular
      la asignatura *cod_asignatura*")
  
- Clases inválidas:
    - `NS1`: BOException("El nif no puede ser nulo")
    - `NS2`: BOException("Nif no válido")
    - `NS3`: BOException("Error al obtener los datos del alumno")
    - `NS4`: BOException("Error en el alta del alumno")
    - `NS5`: BOException("Faltan las asignaturas de matriculación) - Lista vacía o null
    - `NS6`: BOException("El alumno con nif *nif_alumno* ya está matriculado en la asignatura con código *código_asignatura*)
    - `NS7`: BOException("El número máximo de asignaturas es cinco")
  
### Casos de prueba

#### Asunciones

1. `nif`
    1. `12345678A` es un nif que ya está registrado en la base de datos
    2. `11223344B` es un nif que todavía no ha sido registrado en la base de datos
    3. `00000000C` es un nif no válido
  
2. `asignaturas.codigo`
    1. `A01`, `A02`, `A03`, `A04`, `A05`, `A06` son asignaturas a las que nadie se ha matriculado
    2. `A07` es una asignatura a la que todo el mundo se ha matriculado ya
  
#### Tabla

|       Clases       | Dato de entrada 1 `alumno` | Dato de entrada 2 `asignaturas` | Dato de entrada 3 `acceso a BD` | Resultado esperado `MatriculaTO` |                                                                  |                                                                                     |                                     `BOException`                                     |
|:------------------:|:--------------------------:|:-------------------------------:|:-------------------------------:|:--------------------------------:|:----------------------------------------------------------------:|:-----------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------:|
|                    |            `nif`           |                                 |                                 |             `alumno`             |                           `asignaturas`                          |                                      `errores`                                      |                                                                                       |
|  ***V1-V3-V4-S1*** |          12345678A         |    {A01, A02, A03, A04, A05}    |                OK               |         {12345678A; ...}         | {[A01; ...],  [A02; ...],  [A03; ...],  [A04; ...],  [A05; ...]} |                                          {}                                         |                                                                                       |
|  ***V2-V3-V4-S1*** |          11223344B         |    {A01, A02, A03, A04, A05}    |                OK               |         {11223344B; ...}         | {[A01; ...],  [A02; ...],  [A03; ...],  [A04; ...],  [A05; ...]} |                                          {}                                         |                                                                                       |
| ***N1-V3-V4-NS2*** |          00000000C         |    {A01, A02, A03, A04, A05}    |                OK               |                                  |                                                                  |                                                                                     |                                    "Nif no válido"                                    |
| ***N2-V3-V4-NS1*** |            null            |    {A01, A02, A03, A04, A05}    |                OK               |                                  |                                                                  |                                                                                     |                               "El nif no puede ser nulo"                              |
| ***V1-N3-V4-NS5*** |          12345678A         |                {}               |                OK               |                                  |                                                                  |                                                                                     |                         "Faltan asignaturas de matriculación"                         |
| ***V1-N4-V4-NS5*** |          12345678A         |              *null*             |                OK               |                                  |                                                                  |                                                                                     |                         "Faltan asignaturas de matriculación"                         |
| ***V1-N5-V4-NS6*** |          12345678A         |    {A01, A02, A03, A04, A07}    |                OK               |                                  |                                                                  |                                                                                     | "El alumno con nif *12345678A* ya está matriculado de la asignatura con código *A07*" |
| ***V1-N6-V4-NS7*** |          12345678A         |  {A01, A02, A03, A04, A05, A06} |                OK               |                                  |                                                                  |                                                                                     |                       "El número máximo de asignaturas es cinco"                      |
| ***V1-V3-N7-NS3*** |          12345678A         |    {A01, A02, A03, A04, A05}    |              Fallo              |                                  |                                                                  |                                                                                     |                        "Error al obtener los datos del alumno"                        |
| ***V2-V3-N8-NS4*** |          11223344B         |    {A01, A02, A03, A04, A05}    |              Fallo              |                                  |                                                                  |                                                                                     |                             "Error en el alta del alumno"                             |
|  ***V1-V3-N9-S2*** |          12345678A         |    {A01, A02, A03, A04, A05}    |              Fallo              |         {12345678A; ...}         |              {[A01; ...],  [A03; ...],  [A05; ...]}              | {"Error al matricular la asignatura C02",  "Error al matricular la asignatura C04"} |                                                                                       |
