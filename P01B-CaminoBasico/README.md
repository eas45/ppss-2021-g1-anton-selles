
# Soluciones

Tabla de contenido:

* [Ejercicio 1](#markdown-header-ejercicio-1)
    * [Grafo](#markdown-header-grafo-calculatasamatricula)
* [Ejercicio 2](#markdown-header-ejercicio-2)
    * [A)](#markdown-header-a-diseno-de-casos-de-prueba-de-buscartramollanomaslargo)
    * [B)](#markdown-header-b-datos-de-entrada-para-casos-de-prueba)
    * [C)](#markdown-header-c-correccion-de-sentencias-que-no-se-ejecutan)
* [Ejercicio 3](#markdown-header-ejercicio-3)
* [Anotaciones/Correcciones](#markdown-header-anotacionescorrecciones)

- - -

[id_grafo1]: Ejercicio1/CFG-Ejercicio1.png "CFG Ejercicio 1"
[id_grafo2]: Ejercicio2/CFG-Ejercicio2.png "CFG Ejercicio 2"
[id_grafo2corregido]: Ejercicio2/CFG-Ejercicio2Corregido.png "CFG Ejercicio 2 Corregido"
[id_grafo3]: Ejercicio3/CFG-Ejercicio3.png "CFG Ejercicio 3"

## Ejercicio 1

### Diseño de casos de prueba con método del camino básico

```java
1. public float calculaTasaMatricula(int edad, boolean familiaNumerosa,
        2.                                              boolean repetidor) {
        3. float tasa = 500.00f;
        4.
        5. if ((edad < 25) && (!familiaNumerosa) && (repetidor)) {
        6.      tasa = tasa + 1500.00f;
        7. } else {
        8.      if ((familiaNumerosa) || (edad >= 65)) {
        9.          tasa = tasa / 2;
        10.     }
        11.     if ((edad > 50) && (edad < 65)) {
        12.         tasa = tasa - 100.00f;
        13.     }
        14. }
        15. return tasa;
        16.}
```

#### Nodo 1 (Nodo inicial)

> `3.` float tasa = 500.00f;
>
> `4.`
>
> `5.` if ((edad < 25) ~~&& (!familiaNumerosa) && (repetidor)) {~~

*Estas dos instrucciones se ejecutan secuencialmente.*

#### Nodo 2

> `5.` ~~if ((edad < 25)~~ && (!familiaNumerosa) ~~&& (repetidor)) {~~

*Solo si la anterior instrucción es `true` se llegará a este nodo.*

#### Nodo 3

> `5.` ~~if ((edad < 25) && (!familiaNumerosa)~~ && (repetidor)) {

*Solo si también la anterior instrucción es `true` se llegará a este nodo.*

#### Nodo 4

> `6.` tasa = tasa + 1500.00f;

#### Nodo 5

> `7.` } else {
>
> `8.` if ((familiaNumerosa) ~~|| (edad >= 65)) {~~

*Se alcanza este nodo si alguna de las instrucciones de los* ***nodos 1, 2 y 3*** *han dado `false`.*

*Como anteriormente, se ejecutan ambas líneas secuencialmente, pero solo hasta la primera instrucción de la línea 8.*

*Con solo ser `true` esta instrucción, se pasará al nodo que equivale a la línea de dentro del `if`.*

#### Nodo 6

> `8.` ~~if ((familiaNumerosa)~~ || (edad >= 65)) {

#### Nodo 7

> `9.` tasa = tasa / 2;

#### Nodo 8

> `10.` }
>
> `11.` if ((edad > 50) ~~&& (edad < 65)) {~~

#### Nodo 9

> `11.` ~~if ((edad > 50)~~ && (edad < 65)) {

#### Nodo 10

> `12.` tasa = tasa - 100.00f;

#### Nodo 11 (Nodo final)

> `15.` return tasa;
>
> `16.` }   // Fin de la función

- - -

### Grafo `calculaTasaMatricula`

![Imagen Grafo 1][id_grafo1]

#### Complejidad ciclomática

> - CC = Nº Arcos - Nº Nodos + 2
>
> *Si el código es estructurado, también puede calcularse con:*
>
> - CC = Nº Regiones
> - CC = Nº Condiciones + 1

En este caso, hay un solo return en el código, por lo que **sí** es estructurado y puede calcularse de las 3 formas:

1) CC con **nº de arcos y nodos** = 17 - 11 + 2 = 8

2) CC con **nº de regiones** = 8

3) CC con **nº de condiciones** = 7 + 1 = 8

#### Caminos independientes

| Identificador |      Nodos      |
|:-------------:|:---------------:|
|       C1      |    1-2-3-4-11   |
|       C2      |   1-2-5-6-8-11  |
|       C3      |   1-2-5-7-8-11  |
|       C4      |  1-5-6-7-8-9-11 |
|       C5      | 1-5-7-8-9-10-11 |

#### Casos de prueba

| Identificador de Casos de prueba *-Caminos-* | Dato de entrada 1 `edad` | Dato de entrada 2 `familiaNumerosa` | Dato de entrada 3 `repetidor` | Resultado esperado |
|:--------------------------------------------:|:------------------------:|:-----------------------------------:|:-----------------------------:|:------------------:|
|                      C1                      |            19            |                false                |              true             |       2000.00      |
|                      C2                      |            20            |                false                |             false             |       500.00       |
|                      C3                      |            23            |                 true                |              true             |       250.00       |
|                      C4                      |            67            |                false                |              true             |       250.00       |
|                      C5                      |            52            |                 true                |             false             |       400.00       |

- - -

## Ejercicio 2

### A) Diseño de casos de prueba de `buscarTramoLlanoMasLargo()`

```java
1. public Tramo buscarTramoLlanoMasLargo(ArrayList<Integer> lecturas) {
        2.   int lectura_anterior =-1;
        3.   int longitud_tramo =0, longitudMax_tramo=0;
        4.   int origen_tramo=-1, origen_tramoMax=-1;
        5.   Tramo resultado = new Tramo(); //el origen y la longitud es CERO
        6.
        7.   for(Integer dato:lecturas) {
        8.     if (lectura_anterior== dato) {//detectamos un llano
        9.       longitud_tramo ++;
        10.      if (origen_tramo == -1 ) {//marcamos su origen
        11.        origen_tramo = lecturas.indexOf(dato);
        12.      }
        13.    } else { //no es un llano o se termina el tramo llano
        14.      longitud_tramo=0;
        15.      origen_tramo=-1;
        16.    }
        17.    //actualizamos la longitud máxima del llano detectado
        18.    if (longitud_tramo > longitudMax_tramo) {
        19.      longitudMax_tramo = longitud_tramo;
        20.      origen_tramoMax = origen_tramo;
        21.    }
        22.    lectura_anterior=dato;
        23.  }
        24.  switch (longitudMax_tramo) {
        25.    case -1:
        26.    case 0: break;
        27.    default: resultado.setOrigen(origen_tramoMax);
        28.             resultado.setLongitud(longitudMax_tramo);
        29.  }
        30.
        31.  return resultado;
        32. }
```

- - -

#### Grafo `buscarTramoLlanoMasLargo`

![Imagen Grafo 2][id_grafo2]

#### Complejidad ciclomática

1) CC con **nº de arcos y nodos** = 20 - 15 + 2 = 7

2) CC con **nº de regiones** = 7

3) CC con **nº de condiciones** = 6 + 1 = 7

> **¡OJO!**
>
> En el `switch`, el número de condiciones es el número de casos que hay. `default` no es una condición, ya que se
> ejecuta en el caso de que no se coincida con algún caso.
>
> Por eso, en este caso, el `switch` contaría como 2 condiciones, en las que se comprueba si es -1 o 0.

#### Caminos independientes

| Identificador |                         Nodos                         |
|:-------------:|:-----------------------------------------------------:|
|       C1      | 1-2-3-7-8-10-2-3-4-5-6-8-9-10-2-3-4-5-8-10-2-11-14-15 |
|       C2      |                1-2-3-7-8-10-2-11-13-15                |
|       C2      |               1-2-3-7-8-10-2-11-12-13-15              |

### B) Datos de entrada para casos de prueba

| Identificador de Casos de prueba *-Caminos-* | Dato de entrada 1 `lecturas` |
|:--------------------------------------------:|:----------------------------:|
|                      C1                      |         [10, 10, 10]         |
|                      C2                      |             [10]             |
|                      C3                      |               -              |

El nodo 12 no se recorre porque es imposible, ya que `longitudMax_tramo` no puede valer -1 en la implementación. Esto
hace que el camino *C3* no se pueda recorrer porque no existe un entrada de datos que permita realizarlo.

Se debe revisar la implementación y reorganizar el grafo.

### C) Corrección de sentencias que no se ejecutan

```java
24.  switch (longitudMax_tramo) {
        25.    //case -1:
        26.    case 0: break;
        27.    default: resultado.setOrigen(origen_tramoMax);
        28.             resultado.setLongitud(longitudMax_tramo);
        29.  }
```

Con eliminar la sentencia de la línea 25, se resolvería el problema que se tenía anteriormente.

#### Grafo `buscarTramoLlanoMasLargo` corregido

![Imagen Grafo 2 Corregido][id_grafo2corregido]

#### Complejidad ciclomática

1) CC con **nº de arcos y nodos** = 18 - 14 + 2 = 6

2) CC con **nº de regiones** = 6

3) CC con **nº de condiciones** = 5 + 1 = 6

#### Caminos independientes

| Identificador |                         Nodos                         |
|:-------------:|:-----------------------------------------------------:|
|       C1      | 1-2-3-7-8-10-2-3-4-5-6-8-9-10-2-3-4-5-8-10-2-11-13-14 |
|       C2      |                1-2-3-7-8-10-2-11-12-14                |

#### Tabla de casos de prueba

| Identificador de Casos de prueba *-Caminos-* | Dato de entrada 1 `lecturas` | Resultado esperado `resultado` |
|:--------------------------------------------:|:----------------------------:|:------------------------------:|
|                      C1                      |         [10, 10, 10]         |  `origen` = 0, `longitud` = 2  |
|                      C2                      |             [10]             |  `origen` = 0, `longitud` = 0  |

- - -

> **IMPORTANTE**
> 
> **No se puede asegurar que el código no tenga defectos**. Con el método utilizado solo se sabe que no se ha podido
> encontrar errores, pero no quiere decir que no los haya. Es una afirmación que no se puede hacer con este método.

## Ejercicio 3

### Diseño de casos de prueba de `realizaReserva()`

```java
1.public void realizaReserva(String login, String password,
2.                          String socio, String [] isbns) throws Exception {
3.  ArrayList<String> errores = new ArrayList<String>();
4.  //El método compruebaPermisos() devuelve cierto si la persona que hace
5.  //la reserva es el bibliotecario y falso en caso contrario
6.  if(!compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)) {
7.      errores.add(“ERROR de permisos”);
8.  } else {
9.      FactoriaBOs fd = FactoriaBOs.getInstance();
10.     //El método getOperacionBO() devuelve un objeto de tipo IOperacionBO
11.     //a partir del cual podemos hacer efectiva la reserva
12.     IOperacionBO io = fd.getOperacionBO();
13.     try {
14.         for(String isbn: isbns) {
15.             try {
16.                 //El método reserva() registra la reserva de un libro (para un socio)
17.                 //dados el identificador del socio e isbn del libro a reservar
18.                 io.reserva(socio, isbn);
19.             } catch (IsbnInvalidoException iie) {
20.                 errores.add(“ISBN invalido” + “:” + isbn);
21.             }
22.         }
23.     } catch (SocioInvalidoException sie) {
24.         errores.add(“SOCIO invalido”);
25.     } catch (JDBCException je) {
26.         errores.add(“CONEXION invalida”);
27.     }
28. }
29. if (errores.size() > 0) {
30.     String mensajeError = “”;
31.     for(String error: errores) {
32.         mensajeError += error + “; “;
33.     }
34.     throw new ReservaException(mensajeError);
35. }
36.}
```

#### Grafo `realizaReserva`

![Imagen Grafo 3][id_grafo3]

#### Complejidad ciclomática

- CC con **nº de arcos y nodos** = 22 - 16 + 2 = 8

Las otras dos formas de calcular la complejidad ciclomática no son válidas en este caso porque los `try - catch` y el
`throw` hacen que el código tenga saltos incondicionales, por lo que el código no es estructurado.

#### Caminos independientes

| Identificador |                 Nodos                 |
|:-------------:|:-------------------------------------:|
|       C1      |        1-2-3-11-12-13-14-13-16        |
|       C2      |          1-2-4-5-6-7-6-11-16          |
|       C3      |   1-2-4-5-6-7-8-11-12-13-14-13-15-16  |
|       C4      |   1-2-4-5-6-7-8-11-12-13-14-13-15-16  |
|       C5      | 1-2-4-5-6-7-10-6-11-12-13-14-13-15-16 |

#### Tabla de casos de prueba

| Identificador de Casos de prueba *-Caminos-* | Dato de entrada 1 `login` | Dato de entrada 2 `password` | Dato de entrada 3 `socio` | Dato de entrada 4 `isbns` |  Resultado esperado |
|:--------------------------------------------:|:-------------------------:|:----------------------------:|:-------------------------:|:-------------------------:|:-------------------:|
|                      C1                      |           biblia          |             1234             |           socio1          |          [12345]          |          ?          |
|                      C2                      |           biblio          |             1234             |           socio1          |          [12345]          |                     |
|                      C3*                     |           biblio          |             1234             |           socio1          |          [12345]          |  CONEXION invalida  |
|                      C4                      |           biblio          |             1234             |           sucio1          |          [12345]          |    SOCIO invalido   |
|                      C5                      |           biblio          |             1234             |           socio1          |          [23456]          | ISBN invalido:23456 |

(*) En el caso de C3, se supone que el fallo debe ser con la conexión. No hay una entrada que pueda provocar esto.

- - -

# Anotaciones/Correcciones

## Ejercicio 3

- Añadir columna sobre lo que ocurre con el método que lanza las excepciones. Podría llamarse "reserva()".

- Hacer un apunte sobre los datos aceptados 