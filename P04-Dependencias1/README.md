# Solución

Tabla de contenido:

* [Ejercicio 1](#markdown-header-ejercicio-1)
* [Ejercicio 2](#markdown-header-ejercicio-2)
* [Ejercicio 3](#markdown-header-ejercicio-3)
* [Ejercicio 4](#markdown-header-ejercicio-4)
* [Anotaciones/Correcciones](#markdown-header-anotacionescorrecciones)

- - -

## Conceptos previos

* En esta práctica los drivers implementados realizan una **verificación basada en el estado**.
  
* Las **dependencias externas** se sustituirán por sus dobles, concretamente por STUBS.

* La idea es que el doble controle las **entradas indirectas**.

* NO se puede alterar el código de la SUT *temporalmente*.

* Se puede **refactorizar** para que el código contenga un **seam**, uno por cada dependencia externa.

* El seam permite la inyección del doble durante las pruebas, reemplazando al colaborador (DOC).

* El algoritmo en la implementación del los drivers es:
    1. Preparar datos de entrada.
        1. **Añadir** acciones en la fase de preparación de datos
        2. **Crear** el doble
        3. **Programar el resultado** del doble
        4. **Inyectar** el doble en nuestra SUT
    2. Invocar el SUT
    3. Verificar el resultado
    4. Restaurar los datos

## Ejercicio 1

```java
package ppss.ejercicio1;

import java.util.Calendar;

public class GestorLlamadas {
  static double TARIFA_NOCTURNA=10.5;
  static double TARIFA_DIURNA=20.8;
  
  public int getHoraActual() {
    Calendar c = Calendar.getInstance();
    int hora = c.get(Calendar.HOUR);
    return hora;
  }

  public double calculaConsumo(int minutos) {
    int hora = getHoraActual();
    if(hora < 8 || hora > 20) {
      return minutos * TARIFA_NOCTURNA;
    } else {
      return minutos * TARIFA_DIURNA;
    }
  }
}
```

### Paso 1 - Conocer las *dependencias*

La dependencia externa de la SUT es el método `getHoraActual()`.

### Paso 2 - Asegurar si es *testable*

El código es testable, ya que la forma de recibir los valores de los que depende es:

1. A través de un **argumento** que le entra a la SUT - `(int minutos)`

2. A través de un método de **factoría local** - `int hora = getHoraActual();`

Esta línea es la primera lína del método *calculaConsumo(int minutos)*.

```java
int hora = getHoraActual();
```

### Paso 3 - Implementar el *doble*

En la ruta parcial del proyecto `src\test\java` se crea la clase **GestorLlamadasStub** y se implementa el doble.

```java
package ppss.ejercicio1;

public class GestorLlamadasStub extends GestorLlamadas {
  int result;

  public void setResult(int result) {
    this.result = result;
  }

  // Código del doble
  @Override public int getHoraActual() {
    return result;
  }
}
```

### Paso 4 - Implementar los *drivers*

#### Casos de prueba

|          | minutos | hora | Resultado esperado |
|----------|---------|------|--------------------|
| ***C1*** | 10      | 15   | 208                |
| ***C2*** | 10      | 22   | 105                |

#### Implementación

```java
GestorLlamadasStub stub;
double resultadoEsperado;
double resultadoReal;

@BeforeEach public void inicializar() {
  stub = new GestorLlamadasStub();
}

@Test
public void C1_calculaConsumo() {
  resultadoEsperado = 208.0;
  // Se prepara el stub (getHoraActual)
  stub.setResult(15);
  // Se ejecuta la SUT desde el Stub
  resultadoReal = stub.calculaConsumo(10);
  // Se compara si el resultado es el esperado
  assertEquals(resultadoEsperado, resultadoReal);
}

@Test
public void C2_calculaConsumo() {
  resultadoEsperado = 105.0;
  stub.setResult(22);
  resultadoReal = stub.calculaConsumo(10);
  assertEquals(resultadoEsperado, resultadoReal);
}
```

El *stub* hace que cuando en la SUT llame al método `getHoraActual()` se pueda controlar qué valor devuelve. Por eso,
se hace uso del set antes de llamar a la SUT desde el *stub*.

Al usar la herencia, la SUT es exactamente igual, es en `getHoraActual()` donde se consigue inyectar el resultado que se
quiere sin modificar el código.

- - -

## Ejercicio 2

- Clase Calendario:
```java
public class Calendario {
  public int getHoraActual() {
    throw new UnsupportedOperationException ("Not yet implemented");
  }
}
```

- Clase GestorLlamadas:
```java
public class GestorLlamadas {
  static double TARIFA_NOCTURNA=10.5;
  static double TARIFA_DIURNA=20.8;

  public Calendario getCalendario() {
    Calendario c = new Calendario();
    return c;
  }

  public double calculaConsumo(int minutos) {
    Calendario c = getCalendario();
    int hora = c.getHoraActual();
    if(hora < 8 || hora > 20) {
      return minutos * TARIFA_NOCTURNA;
    } else {
      return minutos * TARIFA_DIURNA;
    }
  }
}
```

### Paso 1 - Conocer las *dependencias*

> *ANTES*
> 
> Las dependencias de la SUT están en la primera línea del método. Además, se llama al constructor de la clase Calendario
> desde el método al que se llama, pero esta llamada se realiza desde fuera de la SUT.

Las dependencias de la SUT son las siguientes:

- **getCalendario()** *(Línea 13)* - Llama a una función de la clase GestorLlamadas.
- **getHoraActual()** *(Línea 14)* - Llama a una función de la clase Calendario.

### Paso 2 - Asegurar si es *testable*

> *ANTES*
> 
> De primeras, la SUT no es testable, pues no hay forma de inyectar el doble para que la hora sea la que se quiera al realizar
> el test. Por ello, habrá que **refactorizar**.
> 
> La manera en que se puede refactorizar el código es haciendo que el calendario se reciba como argumento del SUT.
> 
> ```java
> public double calculaConsumo(int minutos, Calendario c) {
>   //Calendario c = getCalendario();
>   int hora = c.getHoraActual();
>   if(hora < 8 || hora > 20) {
>     return minutos * TARIFA_NOCTURNA;
>   } else {
>     return minutos * TARIFA_DIURNA;
>   }
> }
> ```
> 
> Dejaría de ser necesario el método `getCalendario()`.

La SUT es testable debido ambas líneas donde existe la dependencia son seams.

La forma de inyectar el doble es a partir de una clase GestorLlamadasTestable con el método getCalendario sobrescrito para
que se pueda usar la clase CalendarioStub donde se encuentra el código del doble.

### Paso 3 - Implementar el doble

> *ANTES*
> 
> El stub implementado ha sido el de CalendarioStub.
> 
> ```java
> public class CalendarioStub extends Calendario {
>   int result;
> 
>   public CalendarioStub(int result) {
>     this.result = result;
>   }
> 
>   @Override
>   public int getHoraActual() {
>     return result;
>   }
> }
> ```

- Stub implementado para la clase **Calendario**.

```java
public class CalendarioStub extends Calendario {
  int horaActual;

  public void setHoraActual(int hora) {
    horaActual = hora;
  }

  @Override
  public int getHoraActual() {
    return horaActual;
  }
}
```

- Clase testable de **GestorLlamadas**.

```java
public class GestorLlamadasTestable extends GestorLlamadas {

  @Override
  public Calendario getCalendario() {
    Calendario c = new CalendarioStub();
    return c;
  }
}
```

### Paso 4 - Implementar los drivers

```java
class GestorLlamadasTest {

  CalendarioStub calendarioStub;
  double resultadoEsperado, resultadoReal;
  GestorLlamadas gestor;

  @BeforeEach public void inicializacion() {
    gestor = new GestorLlamadas();
  }

  @Test
  void C1_calculaConsumo() {
    resultadoEsperado = 208.0;
    calendarioStub = new CalendarioStub(15);
    resultadoReal = gestor.calculaConsumo(10, calendarioStub);

    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  void C2_calculaConsumo() {
    resultadoEsperado = 105.0;
    calendarioStub = new CalendarioStub(22);
    resultadoReal = gestor.calculaConsumo(10, calendarioStub);

    assertEquals(resultadoEsperado, resultadoReal);
  }
}
```

- - -

## Ejercicio 3

```java
public class AlquilaCoches {
  protected Calendario calendario = new Calendario();

  public Ticket calculaPrecio(TipoCoche tipo, LocalDate inicio, int ndias)
          throws MensajeException {
    Ticket ticket = new Ticket();
    float precioDia,precioTotal =0.0f;
    float porcentaje = 0.25f;

    String observaciones = "";
    IService servicio = new Servicio();
    precioDia = servicio.consultaPrecio(tipo);
    for (int i=0; i<ndias;i++) {
      LocalDate otroDia = inicio.plusDays((long)i);
      try {
        if (calendario.es_festivo(otroDia)) {
          precioTotal += (1+ porcentaje)*precioDia;
        } else {
          precioTotal += (1- porcentaje)*precioDia;
        }
      } catch (CalendarioException ex) {
        observaciones += "Error en dia: "+otroDia+"; ";
      }
    }

    if (observaciones.length()>0) {
      throw new MensajeException(observaciones);
    }

    ticket.setPrecio_final(precioTotal);
    return ticket;
  }
}
```

### Paso 1 - Conocer las *dependencias*

> *ANTES*
> 
> La SUT contiene varias dependencias externas:
>
> * En el atributo `calendario` de la clase *Calendario*.
> * En los argumentos de entrada del método:
>     * `tipo`
>     * `inicio`
>     * `servicio`

Las dependencias de la SUT son las siguientes:

- **consultaPrecio(tipo)** - El método pertenece a la clase Servicio.
- **es_festivo(otroDia)** - El método pertenece a la clase Calendario.

### Paso 2 - Asegurar si es *testable*

> *ANTES*
> 
> En un principio, la SUT no es testable. Esto es debido a que no se puede lograr inyectar el doble si el atributo `calendario`
> es creado desde su declaración en la clase.
> 
> La forma en que se puede **refactorizar** el código para conseguir un seam, puede ser la siguiente:
> 
> ```java
>   protected Calendario calendario;// = new Calendario();
> 
>   public AlquilaCoches(Calendario c) {
>     calendario = c;
>   }
> ```
> Ahora sí que se tiene un *seam* introducir el código del doble desde los tests, ya que se podrá crear el objeto con la clase
> *Calendario* que se quiera.

Como se dispone el código de la SUT, de primeras, no es testable. Para poder usar el doble ServicioStub, se debe refactorizar
y eliminar la línea que hace uso del `new` en la SUT.

Como el atributo calendario es protegido, con una herencia desde la clase AlquilaCoches se podrá optar por la construcción
del atributo con una clase heredada de la clase Calendario.

El resultado de la refactorización es el siguiente, un nuevo método a modo de factoría local y un cambio en la línea donde
estaba el `new`:

```java
  public IService getServicio() {
    IService servicio = new Servicio();
    return servicio;
  }

    IService servicio = getServicio();
```

### Paso 3 - Implementar el *doble*

- Stub de la clase **Calendario**.
```java
public class CalendarioStub extends Calendario {
  ArrayList<String> diasTrue = new ArrayList<>();
  ArrayList<String> diasException = new ArrayList<>();

  @Override
  public boolean es_festivo(LocalDate otroDia) throws CalendarioException {
    boolean result = false;

    if (diasTrue.contains(otroDia.toString())) {
      result = true;
    } else if (diasException.contains(otroDia.toString())) {
      throw new CalendarioException();
    }

    return result;
  }

  public void setDiasTrue(ArrayList<String> diasTrue) {
    this.diasTrue = diasTrue;
  }

  public void setDiasException(ArrayList<String> diasException) {
    this.diasException = diasException;
  }
}
```
Para poder elegir qué días se quiere tener un tipo de respuesta, en el stub hay dos atributos declarados. Si las listas
están vacías, todos los días serán `false`.

- Stub implementado para la clase **Servicio**, que implementa la interfaz IService.
```java
public class ServicioStub implements IService {
  @Override
  public float consultaPrecio(TipoCoche tipo) {
    float precio = 0.0f;

    if (tipo.equals(TipoCoche.CARAVANA) ||
            tipo.equals(TipoCoche.TURISMO)) {
      precio = 10.0f;
    }

    return precio;
  }
}
```

- Clase testable de **AlquilaCoches**.
```java
public class AlquilaCochesTestable extends AlquilaCoches {

  public AlquilaCochesTestable() {
    calendario = new CalendarioStub();
  }

  @Override
  public IService getServicio() {
    IService servicio = new ServicioStub();
    return servicio;
  }

  public void setCalendario(CalendarioStub calendario) {
    this.calendario = calendario;
  }
}
```

### Paso 4 - Implementación de los *drivers*

#### Casos de prueba

|    Id    | Datos de entrada |               |        |                                                             |                                 Resultado esperado                                     |
|:--------:|:----------------:|:-------------:|:------:|:-----------------------------------------------------------:|:--------------------------------------------------------------------------------------:|
|          |      *Tipo*      | *fechaInicio* | *dias* |                          *festivo*                          |                       *Ticket (importe) o MensajeException*                            |
| ***C1*** |      TURISMO     |   2021-05-18  |   10   |                  false para todos los días                  |                                     Ticket(75)                                         |
| ***C2*** |     CARAVANA     |   2021-06-19  |    7   |               true solo para los días 20 y 24               |                                    Ticket(62,5)                                        |
| ***C3*** |      TURISMO     |   2021-04-17  |    8   | false para todos los días, y lanza excepción en 18, 21 y 22 | (“Error en dia: 2021-04-18;   Error	en dia:	2021-04-21;   Error	en dia:	2021-04-22; ”)|

#### Implementación

```java
class AlquilaCochesTest {

  Ticket resultadoEsperado;
  Ticket resultadoReal;

  @BeforeEach public void inicializacion() {
    resultadoEsperado = new Ticket();
    resultadoReal = new Ticket();
  }

  @Test
  void C1_calculaPrecio() {
    // Se indica el resultado esperado
    resultadoEsperado.setPrecio_final(75.0f);
    LocalDate fecha = LocalDate.of(2021, Month.MAY, 18);
    // Se utiliza la clase testable
    AlquilaCochesTestable alquilaCoches = new AlquilaCochesTestable();

    assertDoesNotThrow(() -> resultadoReal = alquilaCoches.calculaPrecio(TipoCoche.TURISMO, fecha, 10));
    assertEquals(resultadoEsperado.getPrecio_final(), resultadoReal.getPrecio_final());
  }

  @Test
  void C2_calculaPrecio() {
    // Se prepara el resultado
    resultadoEsperado.setPrecio_final(62.5f);
    // Se prepara el stub
    ArrayList<String> diasTrue = new ArrayList<>(Arrays.asList("2021-06-19", "2021-06-24"));
    CalendarioStub calendarioStub = new CalendarioStub();
    calendarioStub.setDiasTrue(diasTrue);
    // Se prepara la clase testable
    LocalDate fecha = LocalDate.of(2021, Month.JUNE, 19);
    AlquilaCochesTestable alquilaCoches = new AlquilaCochesTestable();
    // Se inyecta el doble
    alquilaCoches.setCalendario(calendarioStub);

    assertDoesNotThrow(() -> resultadoReal = alquilaCoches.calculaPrecio(TipoCoche.CARAVANA, fecha, 7));
    assertEquals(resultadoEsperado.getPrecio_final(), resultadoReal.getPrecio_final());
  }

  @Test
  void C3_calculaPrecio() {
    String resultadoEsperado = "Error en dia: 2021-04-18; Error en dia: 2021-04-21; Error en dia: 2021-04-22; ";
    // Se crea el doble
    ArrayList<String> diasException = new ArrayList<>(Arrays.asList("2021-04-18", "2021-04-21", "2021-04-22"));
    CalendarioStub calendarioStub = new CalendarioStub();
    calendarioStub.setDiasException(diasException);
    // Se inyecta en la clase testable
    LocalDate fecha = LocalDate.of(2021, Month.APRIL, 17);
    AlquilaCochesTestable alquilaCoches = new AlquilaCochesTestable();
    alquilaCoches.setCalendario(calendarioStub);

    Exception exception = assertThrows(MensajeException.class,
            () -> alquilaCoches.calculaPrecio(TipoCoche.TURISMO, fecha, 8));
    assertEquals(resultadoEsperado, exception.getMessage());
  }
}
```

> ***DUDA***
> 
> Al asumirse un valor desde un método que se implementa en otra clase, cosa que hace que sea una dependecia externa,
> ¿debería refactorizar para poder inyectar el código sobre la asunción?

- - -

## Ejercicio 4

```java
public class Reserva {

  public boolean compruebaPermisos(String login, String password, Usuario tipoUsu) {
    throw new UnsupportedOperationException("Not yet implemented");
  }

  public void realizaReserva(String login, String password,
                             String socio, String [] isbns) throws Exception {

    ArrayList<String> errores = new ArrayList<>();
    if(!compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)) {
      errores.add("ERROR de permisos");
    } else {
      IOperacionBO io = new Operacion();
      try {
        for(String isbn: isbns) {
          try {
            io.operacionReserva(socio, isbn);
          } catch (IsbnInvalidoException iie) {
            errores.add("ISBN invalido" + ":" + isbn);
          }
        }
      } catch (SocioInvalidoException sie) {
        errores.add("SOCIO invalido");
      } catch (JDBCException je) {
        errores.add("CONEXION invalida");
      }
    }
    if (errores.size() > 0) {
      String mensajeError = "";
      for(String error: errores) {
        mensajeError += error + "; ";
      }
      throw new ReservaException(mensajeError);
    }
  }
}
```

### Paso 1 - Conocer las *dependencias*

- Método de la clase Reserva - `compruebaPermisos`

- Método de la clase Operacion - `operacionReserva`

### Paso 2 - Asegurar si es *testable*

De las dependencias externas, no hay un **seam** del método `operacionReserva`. Por lo que se ve, con el uso de una
***factoría local*** se podría crear el **seam** para esta dependencia.

La manera en que quedaría refactorizado sería la siguiente:

```java
// Factoría local
public IOperacionBO getOperacion() {
  IOperacionBO operacion = new Operacion();
  return operacion;
}

// Cambio primera línea del else de la SUT
        //IOperacionBO io = new Operacion();
        IOperacionBO io = getOperacion();
```

### Paso 3 - Implementar el *doble*

Para poder implementar los dobles de la clase Reserva y la clase Operación, se han creado dos nuevas clases:

- Clase *ReservaTestable* - Con el uso del setter, se podrá usar el obejto IOperacionDB que se quiera.
```java
public class ReservaStub extends Reserva {

  IOperacionBO operacion;
  boolean result;

  public ReservaStub(boolean result) {
    this.result = result;
    operacion = new OperacionStub();
  }

  @Override
  public IOperacionBO getOperacion() {
    return operacion;
  }

  public void setOperacion(IOperacionBO operacion) {
    this.operacion = operacion;
  }

  @Override
  public boolean compruebaPermisos(String login, String password, Usuario tipoUsu) {
    return result;
  }
}
```

- Clase *OperacionStub* - Se necesita hacer un 
```java
public class OperacionStub implements IOperacionBO {

  boolean isbnException, JDBCException, socioException;

  public OperacionStub() {
    isbnException = JDBCException = socioException = false;
  }

  @Override
  public void operacionReserva(String socio, String isbn)
          throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
    if (isbnException) {
      throw new IsbnInvalidoException();
    } else if (JDBCException) {
      throw new JDBCException();
    } else if (socioException) {
      throw new SocioInvalidoException();
    }
  }

  public void setIsbn(boolean isbn) {
    this.isbnException = isbn;
  }

  public void setJDBC(boolean JDBC) {
    this.JDBCException = JDBC;
  }

  public void setSocio(boolean socio) {
    this.socioException = socio;
  }
}
```

### Paso 4 - Implementar los *drivers*

#### Asunciones

Suponemos que el login/password del bibliotecario es "ppss"/"ppss"; que "Luis" es un socio y "Pepe" no lo es; y que los
isbn registrados en la base datos son "11111", "22222".

- **ReservaException1:** Excepción de tipo ReservaException con el mensaje: "ERROR de permisos; "
- **ReservaException2:** Excepción de tipo ReservaException con el mensaje: "ISBN invalido:33333; "
- **ReservaException3:** Excepción de tipo ReservaException con el mensaje: "SOCIO invalido; "
- **ReservaException4:** Excepción de tipo ReservaException con el mensaje: "CONEXION invalida; "


- **(1):** No se invoca al método reserva()
- **(2):** El método reserva() NO lanza ninguna excepción
- **(3):** El método reserva() lanza la excepción IsbnInvalidoException
- **(4):** El método reserva() lanza la excepción SocioInvalidoException
- **(5):** El método reserva() lanza la excepción ConexiónInvalidaException

#### Casos de prueba

|          |  login | password | ident. socio | Acceso BD |        isbns       | Resultado esperado |
|:--------:|:------:|:--------:|:------------:|:---------:|:------------------:|:------------------:|
| ***C1*** | "xxxx" |  "xxxx"  |    "Luis"    |    (1)    |      {"11111"}     |  ReservaException1 |
| ***C2*** | "ppss" |  "ppss"  |    "Luis"    |    (2)    | {"11111", "22222"} | No se lanza excep. |
| ***C3*** | "ppss" |  "ppss"  |    "Luis"    |    (3)    |      {"33333"}     |  ReservaException2 |
| ***C4*** | "ppss" |  "ppss"  |    "Pepe"    |    (4)    |      {"11111"}     |  ReservaException3 |
| ***C5*** | "ppss" |  "ppss"  |    "Luis"    |    (5)    |      {"11111"}     |  ReservaException4 |

#### Implementación

```java
class ReservaTest {

  ReservaStub reservaStub;
  OperacionStub operacionStub;
  String[] isbns;

  @BeforeEach public void inicializar() {
    operacionStub = new OperacionStub();
  }

  @Test
  void C1_realizaReserva() {
    // Valores
    isbns = new String[]{"11111"};
    // Stubs
    // operacionStub no es necesaria en este test
    reservaStub = new ReservaStub(false);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("xxxx", "xxxx", "Luis", isbns));
    assertEquals("ERROR de permisos; ", exception.getMessage());
  }

  @Test
  void C2_realizaReserva() {
    // Valores
    isbns = new String[]{"11111", "22222"};
    // Stubs
    reservaStub = new ReservaStub(true);

    assertDoesNotThrow(() -> reservaStub.realizaReserva("ppss", "ppss", "Luis", isbns));
  }

  @Test
  void C3_realizarReserva() {
    // Valores
    isbns = new String[]{"33333"};
    // Stubs
    operacionStub.setIsbn(true);
    reservaStub = new ReservaStub(true);
    reservaStub.setOperacion(operacionStub);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("ppss", "ppss", "Luis", isbns));
    assertEquals("ISBN invalido:33333; ", exception.getMessage());
  }

  @Test
  void C4_realizarReserva() {
    // Valores
    isbns = new String[]{"11111"};
    // Stubs
    operacionStub.setSocio(true);
    reservaStub = new ReservaStub(true);
    reservaStub.setOperacion(operacionStub);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("ppss", "ppss", "Pepe", isbns));
    assertEquals("SOCIO invalido; ", exception.getMessage());
  }

  @Test
  void C5_realizarReserva() {
    // Valores
    isbns = new String[]{"11111"};
    // Stubs
    operacionStub.setJDBC(true);
    reservaStub = new ReservaStub(true);
    reservaStub.setOperacion(operacionStub);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("ppss", "ppss", "Luis", isbns));
    assertEquals("CONEXION invalida; ", exception.getMessage());
  }
}
```

- - -

# Anotaciones/Correcciones

La clase Testable *(la clase que contiene la SUT)* contiene la implementación de la clase con la inyección del doble.
La clase Stub es la implementación del doble.

## Ejercicio 2

***No se debe refactorizar nada***. La SUT es testable.

- **getCalendario()** - La entrada indirecta es un objeto Calendario. Hay que crear un doble para calendario.
- **getHoraActual()** - Se quiere recibir la hora.

## Ejercicio 3

Las dependencias externas son solo los métodos a los que se llama desde la SUT, por lo que hay 3:
- `consultaPrecio(tipo)` - Pertenece a la interfaz IService
- `es_festivo(otroDia)` - Pertenece a la clase Calendario

La primera dependencia (*consultaPrecio*) no es un seam, porque el `new` está dentro de la SUT. Puede resolverse como
factoría local.

La segunda dependencia (*es_festivo*) sí es un seam.

## Anotación profesora

Fichero read.me  
Con respecto al algoritmo del driver No es ese. Lo tienes en las transparencias del tema, p.ej. tr 15 de la sesión S04.  
Lo que tú has puesto no es el algoritmo del driver, forma parte del paso uno del algoritmo: Preparar los datos de entrada
En el ejercicio 2: la SUT sí es testable. No es necesario refactorizar!!! Tienes un ejemplo en la tr. 4 de la sesión S04

Ejercicio 3:  
Sólo tienes dos dependencias externas:  
-método consultaPrecio()  
-método es_festivo()

El doble de es_festivo() (método de una clase Calendario) puedes inyectarlo directamente a través del atributo de la clase
ya que es "protected", no necesitas refactorizar para inyectar el doble de es_festivo()

Con respecto a tu duda, supongo que te refieres a la fecha actual. La debes considerar directamente como atributo de la
clase que contiene el doble. Puedes crear todos los atributos que te hagan falta.

Sí necesitas refactorizar para inyectar el doble de consultaPrecio()

Ejercicio 4: no hay que usar una factoria local. En el enunciado se indica claramente que debes usar una CLASE factoría.
Este ejercicio lo veremos en teoría.

| Hecho |                                    Corrección                                    |
|:-----:|:--------------------------------------------------------------------------------:|
| **X** |      Corregir punto *Conceptos previos*. Añadir el algoritmo correctamente.      |
|       |             *Ejercicio 2* - Corregir la refactorización no necesaria.            |
|       | *Ejercicio 3* - Corregir dependencias externas y la refactorización innecesaria. |
|       |     *Ejercicio 4* - No utilizar una factoría local. Utilizar clase factoría.     |