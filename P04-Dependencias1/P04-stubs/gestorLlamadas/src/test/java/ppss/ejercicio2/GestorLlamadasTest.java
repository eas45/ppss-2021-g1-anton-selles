package ppss.ejercicio2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {

  CalendarioStub calendarioStub;
  double resultadoEsperado, resultadoReal;
  GestorLlamadas gestor;

  @BeforeEach public void inicializacion() {
    gestor = new GestorLlamadas();
  }

  @Test
  void C1_calculaConsumo() {
    resultadoEsperado = 208.0;
    calendarioStub = new CalendarioStub(15);
    resultadoReal = gestor.calculaConsumo(10, calendarioStub);

    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  void C2_calculaConsumo() {
    resultadoEsperado = 105.0;
    calendarioStub = new CalendarioStub(22);
    resultadoReal = gestor.calculaConsumo(10, calendarioStub);

    assertEquals(resultadoEsperado, resultadoReal);
  }
}