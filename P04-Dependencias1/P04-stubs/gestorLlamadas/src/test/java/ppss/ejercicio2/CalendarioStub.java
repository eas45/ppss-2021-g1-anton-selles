package ppss.ejercicio2;

public class CalendarioStub extends Calendario {
  int horaActual;

  public void setHoraActual(int hora) {
    horaActual = hora;
  }

  @Override
  public int getHoraActual() {
    return horaActual;
  }
}
