package ppss.ejercicio1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GestorLlamadasTest {

  GestorLlamadasStub stub;
  double resultadoEsperado;
  double resultadoReal;

  @BeforeEach public void inicializar() {
    stub = new GestorLlamadasStub();
  }

  @Test
  public void C1_calculaConsumo() {
    resultadoEsperado = 208.0;
    // Se prepara el stub (getHoraActual)
    stub.setResult(15);
    // Se ejecuta la SUT desde el Stub
    resultadoReal = stub.calculaConsumo(10);
    // Se compara si el resultado es el esperado
    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  public void C2_calculaConsumo() {
    resultadoEsperado = 105.0;
    stub.setResult(22);
    resultadoReal = stub.calculaConsumo(10);
    assertEquals(resultadoEsperado, resultadoReal);
  }
}
