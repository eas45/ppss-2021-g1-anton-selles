package ppss.ejercicio1;

public class GestorLlamadasStub extends GestorLlamadas {
  int result;

  public void setResult(int result) {
    this.result = result;
  }

  // Código del doble
  @Override public int getHoraActual() {
    return result;
  }
}
