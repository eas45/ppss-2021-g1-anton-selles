package ppss;

public class ReservaStub extends Reserva {

  IOperacionBO operacion;
  boolean result;

  public ReservaStub(boolean result) {
    this.result = result;
    operacion = new OperacionStub();
  }

  @Override
  public IOperacionBO getOperacion() {
    return operacion;
  }

  public void setOperacion(IOperacionBO operacion) {
    this.operacion = operacion;
  }

  @Override
  public boolean compruebaPermisos(String login, String password, Usuario tipoUsu) {
    return result;
  }
}
