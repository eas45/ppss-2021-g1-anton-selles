package ppss;

import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.SocioInvalidoException;

public class OperacionStub implements IOperacionBO {

  boolean isbnException, JDBCException, socioException;

  public OperacionStub() {
    isbnException = JDBCException = socioException = false;
  }

  @Override
  public void operacionReserva(String socio, String isbn)
          throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
    if (isbnException) {
      throw new IsbnInvalidoException();
    } else if (JDBCException) {
      throw new JDBCException();
    } else if (socioException) {
      throw new SocioInvalidoException();
    }
  }

  public void setIsbn(boolean isbn) {
    this.isbnException = isbn;
  }

  public void setJDBC(boolean JDBC) {
    this.JDBCException = JDBC;
  }

  public void setSocio(boolean socio) {
    this.socioException = socio;
  }
}
