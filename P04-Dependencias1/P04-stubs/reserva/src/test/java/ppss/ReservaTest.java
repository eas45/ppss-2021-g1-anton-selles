package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ppss.excepciones.ReservaException;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReservaTest {

  ReservaStub reservaStub;
  OperacionStub operacionStub;
  String[] isbns;

  @BeforeEach public void inicializar() {
    operacionStub = new OperacionStub();
  }

  @Test
  void C1_realizaReserva() {
    // Valores
    isbns = new String[]{"11111"};
    // Stubs
    // operacionStub no es necesaria en este test
    reservaStub = new ReservaStub(false);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("xxxx", "xxxx", "Luis", isbns));
    assertEquals("ERROR de permisos; ", exception.getMessage());
  }

  @Test
  void C2_realizaReserva() {
    // Valores
    isbns = new String[]{"11111", "22222"};
    // Stubs
    reservaStub = new ReservaStub(true);

    assertDoesNotThrow(() -> reservaStub.realizaReserva("ppss", "ppss", "Luis", isbns));
  }

  @Test
  void C3_realizarReserva() {
    // Valores
    isbns = new String[]{"33333"};
    // Stubs
    operacionStub.setIsbn(true);
    reservaStub = new ReservaStub(true);
    reservaStub.setOperacion(operacionStub);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("ppss", "ppss", "Luis", isbns));
    assertEquals("ISBN invalido:33333; ", exception.getMessage());
  }

  @Test
  void C4_realizarReserva() {
    // Valores
    isbns = new String[]{"11111"};
    // Stubs
    operacionStub.setSocio(true);
    reservaStub = new ReservaStub(true);
    reservaStub.setOperacion(operacionStub);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("ppss", "ppss", "Pepe", isbns));
    assertEquals("SOCIO invalido; ", exception.getMessage());
  }

  @Test
  void C5_realizarReserva() {
    // Valores
    isbns = new String[]{"11111"};
    // Stubs
    operacionStub.setJDBC(true);
    reservaStub = new ReservaStub(true);
    reservaStub.setOperacion(operacionStub);

    Exception exception = assertThrows(ReservaException.class,
            () -> reservaStub.realizaReserva("ppss", "ppss", "Luis", isbns));
    assertEquals("CONEXION invalida; ", exception.getMessage());
  }
}