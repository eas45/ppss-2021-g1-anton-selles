import java.time.LocalDate;
import java.util.ArrayList;

public class CalendarioStub extends Calendario {
  ArrayList<String> diasTrue = new ArrayList<>();
  ArrayList<String> diasException = new ArrayList<>();

  @Override
  public boolean es_festivo(LocalDate otroDia) throws CalendarioException {
    boolean result = false;

    if (diasTrue.contains(otroDia.toString())) {
      result = true;
    } else if (diasException.contains(otroDia.toString())) {
      throw new CalendarioException();
    }

    return result;
  }

  public void setDiasTrue(ArrayList<String> diasTrue) {
    this.diasTrue = diasTrue;
  }

  public void setDiasException(ArrayList<String> diasException) {
    this.diasException = diasException;
  }
}