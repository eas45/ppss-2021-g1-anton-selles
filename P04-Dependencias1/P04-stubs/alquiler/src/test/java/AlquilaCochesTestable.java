public class AlquilaCochesTestable extends AlquilaCoches {

  public AlquilaCochesTestable() {
    calendario = new CalendarioStub();
  }

  @Override
  public IService getServicio() {
    IService servicio = new ServicioStub();
    return servicio;
  }

  public void setCalendario(CalendarioStub calendario) {
    this.calendario = calendario;
  }
}
