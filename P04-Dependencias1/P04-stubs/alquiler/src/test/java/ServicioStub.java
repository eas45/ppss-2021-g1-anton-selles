public class ServicioStub implements IService {
  @Override
  public float consultaPrecio(TipoCoche tipo) {
    float precio = 0.0f;

    if (tipo.equals(TipoCoche.CARAVANA) ||
        tipo.equals(TipoCoche.TURISMO)) {
      precio = 10.0f;
    }

    return precio;
  }
}
