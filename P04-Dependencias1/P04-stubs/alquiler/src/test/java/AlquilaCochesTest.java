import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class AlquilaCochesTest {

  Ticket resultadoEsperado;
  Ticket resultadoReal;

  @BeforeEach public void inicializacion() {
    resultadoEsperado = new Ticket();
    resultadoReal = new Ticket();
  }

  @Test
  void C1_calculaPrecio() {
    // Se indica el resultado esperado
    resultadoEsperado.setPrecio_final(75.0f);
    LocalDate fecha = LocalDate.of(2021, Month.MAY, 18);
    // Se utiliza la clase testable
    AlquilaCochesTestable alquilaCoches = new AlquilaCochesTestable();

    assertDoesNotThrow(() -> resultadoReal = alquilaCoches.calculaPrecio(TipoCoche.TURISMO, fecha, 10));
    assertEquals(resultadoEsperado.getPrecio_final(), resultadoReal.getPrecio_final());
  }

  @Test
  void C2_calculaPrecio() {
    // Se prepara el resultado
    resultadoEsperado.setPrecio_final(62.5f);
    // Se prepara el stub
    ArrayList<String> diasTrue = new ArrayList<>(Arrays.asList("2021-06-19", "2021-06-24"));
    CalendarioStub calendarioStub = new CalendarioStub();
    calendarioStub.setDiasTrue(diasTrue);
    // Se prepara la clase testable
    LocalDate fecha = LocalDate.of(2021, Month.JUNE, 19);
    AlquilaCochesTestable alquilaCoches = new AlquilaCochesTestable();
    // Se inyecta el doble
    alquilaCoches.setCalendario(calendarioStub);

    assertDoesNotThrow(() -> resultadoReal = alquilaCoches.calculaPrecio(TipoCoche.CARAVANA, fecha, 7));
    assertEquals(resultadoEsperado.getPrecio_final(), resultadoReal.getPrecio_final());
  }

  @Test
  void C3_calculaPrecio() {
    String resultadoEsperado = "Error en dia: 2021-04-18; Error en dia: 2021-04-21; Error en dia: 2021-04-22; ";
    // Se crea el doble
    ArrayList<String> diasException = new ArrayList<>(Arrays.asList("2021-04-18", "2021-04-21", "2021-04-22"));
    CalendarioStub calendarioStub = new CalendarioStub();
    calendarioStub.setDiasException(diasException);
    // Se inyecta en la clase testable
    LocalDate fecha = LocalDate.of(2021, Month.APRIL, 17);
    AlquilaCochesTestable alquilaCoches = new AlquilaCochesTestable();
    alquilaCoches.setCalendario(calendarioStub);

    Exception exception = assertThrows(MensajeException.class,
            () -> alquilaCoches.calculaPrecio(TipoCoche.TURISMO, fecha, 8));
    assertEquals(resultadoEsperado, exception.getMessage());
  }
}