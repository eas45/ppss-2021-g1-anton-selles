# Soluciones

## Ejercicio 1

```java
public class GestorLlamadas {
  static double TARIFA_NOCTURNA=10.5;
  static double TARIFA_DIURNA=20.8;

  public Calendario getCalendario() {
    Calendario c = new Calendario();
    return c;
  }

  public double calculaConsumo(int minutos) {
    Calendario c = getCalendario();
    int hora = c.getHoraActual();
    if(hora < 8 || hora > 20) {
      return minutos * TARIFA_NOCTURNA;
    } else {
      return minutos * TARIFA_DIURNA;
    }
  }
}
```
```java
public class Calendario {
  public int getHoraActual() {
    throw new
            UnsupportedOperationException
            ("Not yet implemented");
  }
}
```

Aunque ahora se pueda crear tanto el **stub** como el **mock** con el framework ***EasyMock***, se debe de seguir controlando
los **seams** que existen en la SUT. De forma que se continúa comprobando las dependencias y si es testable.

### Paso 1 - Conocer las *dependecias*

Las dependencias de la SUT son las siguientes:

- **getCalendario()** - Pertenece a la clase Calendario.
- **getHoraActual()** - Pertenece a la clase GestorLlamadas.

### Paso 2 - Asegurar si es *testable*

La SUT es testable, no hay que refactorizar nada del código original. Lo que sí hay que hacer es crear una clase
GestorLlamadasTestable donde se pueda inyectar el código que se quiere.

```java
public class GestorLlamadasTestable extends GestorLlamadas {
  // Forma de inyectar el stub
  Calendario calendario;

  @Override
  public Calendario getCalendario() {
    return calendario;
  }

  public void setCalendario(Calendario calendario) {
    this.calendario = calendario;
  }
}
```

> En el paso 3 de la práctica anterior se implementaba el doble, pero en este caso eso se hace a través de EasyMock.

### Paso 3 - Implementar el *driver*

En este paso se lleva a cabo tanto la implementación del driver como la del mock.

```java
class GestorLlamadasTest {

  GestorLlamadasTestable gestorLlamadasTestable;
  Calendario calendarioMock;
  double resultadoEsperado, resultadoReal;
  int minutos, hora;

  @BeforeEach void inicializar() {
    gestorLlamadasTestable = new GestorLlamadasTestable();
    // La dependencia solo aparece una vez en la SUT
    calendarioMock = EasyMock.createMock(Calendario.class);
    gestorLlamadasTestable.setCalendario(calendarioMock);
  }

  @Test
  void C1_calculaConsumo() {
    // Datos del caso de prueba
    minutos = 22;
    hora = 10;
    resultadoEsperado = 457.6;
    // Se indica lo esperado en el mock
    EasyMock.expect(calendarioMock.getHoraActual()).andReturn(hora);
    EasyMock.replay(calendarioMock);
    // Se usa la clase testable
    resultadoReal = gestorLlamadasTestable.calculaConsumo(minutos);

    // Se verifica que el comportamiento ha sido correcto
    EasyMock.verify(calendarioMock);
    // Se comparan los resultados
    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  void C2_calculaConsumo() {
    // Datos del caso de prueba
    minutos = 13;
    hora = 21;
    resultadoEsperado = 136.5;
    // Se indica lo esperado en el mock
    EasyMock.expect(calendarioMock.getHoraActual()).andReturn(hora);
    EasyMock.replay(calendarioMock);
    // Se usa la clase testable
    resultadoReal = gestorLlamadasTestable.calculaConsumo(minutos);

    // Se verifica que el comportamiento ha sido correcto
    EasyMock.verify(calendarioMock);
    // Se comparan los resultados
    assertEquals(resultadoEsperado, resultadoReal);
  }
}
```

## Ejercicio 2

### Paso 1 - Conocer las *dependencias*

