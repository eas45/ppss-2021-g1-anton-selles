package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.easymock.EasyMock;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {

  GestorLlamadasTestable gestorLlamadasTestable;
  Calendario calendarioMock;
  double resultadoEsperado, resultadoReal;
  int minutos, hora;

  @BeforeEach void inicializar() {
    gestorLlamadasTestable = new GestorLlamadasTestable();
    // La dependencia solo aparece una vez en la SUT
    calendarioMock = EasyMock.createMock(Calendario.class);
    gestorLlamadasTestable.setCalendario(calendarioMock);
  }

  @Test
  void C1_calculaConsumo() {
    // Datos del caso de prueba
    minutos = 22;
    hora = 10;
    resultadoEsperado = 457.6;
    // Se indica lo esperado en el mock
    EasyMock.expect(calendarioMock.getHoraActual()).andReturn(hora);
    EasyMock.replay(calendarioMock);
    // Se usa la clase testable
    resultadoReal = gestorLlamadasTestable.calculaConsumo(minutos);

    // Se verifica que el comportamiento ha sido correcto
    EasyMock.verify(calendarioMock);
    // Se comparan los resultados
    assertEquals(resultadoEsperado, resultadoReal);
  }

  @Test
  void C2_calculaConsumo() {
    // Datos del caso de prueba
    minutos = 13;
    hora = 21;
    resultadoEsperado = 136.5;
    // Se indica lo esperado en el mock
    EasyMock.expect(calendarioMock.getHoraActual()).andReturn(hora);
    EasyMock.replay(calendarioMock);
    // Se usa la clase testable
    resultadoReal = gestorLlamadasTestable.calculaConsumo(minutos);

    // Se verifica que el comportamiento ha sido correcto
    EasyMock.verify(calendarioMock);
    // Se comparan los resultados
    assertEquals(resultadoEsperado, resultadoReal);
  }
}