package ppss;

public class GestorLlamadasTestable extends GestorLlamadas {
  // Forma de inyectar el stub
  Calendario calendario;

  @Override
  public Calendario getCalendario() {
    return calendario;
  }

  public void setCalendario(Calendario calendario) {
    this.calendario = calendario;
  }
}
