# Soluciones

### Ejercicio 1

#### B) Versiones por defecto de plugins que aparecen el fichero pom.xml

- `maven-surefire-plugin` : 2.12.4

- `maven-compiler-plugin` : 3.1

#### D) Tabla de casos de prueba C1, C2, C3 y C4

| Identificador de Casos de prueba *- Caminos -* | Dato de entrada 1 `a` | Dato de entrada 2 `b` | Dato de entrada 3 `c` | Resultado esperado              |
|------------------------------------------------|-----------------------|-----------------------|-----------------------|---------------------------------|
| C1                                             | 1                     | 1                     | 1                     | Equilatero                      |
| C2                                             | 1                     | 1                     | 11                    | No es un triángulo              |
| C3                                             | 1                     | 2                     | 0                     | Valor fuera del rango permitido |
| C4                                             | 14                    | 10                    | 10                    | Isosceles                       |

### Ejercicio 2

#### C) Ejecución de tests

Uno de ellos no es superado por un error en los *if* que comprueban si los valores están dentro del rango permitido, y además, el mensaje de error estaba mal escrito.

**Solución**

      // if ((c < 1) && (c > 200)) {
      if ((c < 1) || (c > 200)) {
         // return "Valor c sobrepasa del rango permitido";
         return "Valor c fuera del rango permitido";
      }

#### D) Razonamiento sobre los tests implementados

##### 1. ¿Qué tienen en común los tests C1 y C5? ¿Conviene incluir C5?

> **Test C1 ->** a = 1; b = 1; c = 1; resultado esperado = "Equilatero";
> 
> **Test C5 ->** a = 7; b = 7; c = 7; resultado esperado = "Equilatero";

Coinciden en que las tres entradas son el mismo número y se espera la misma salida.

No conviene incluir C5 porque no aporta nada distinto de C1. Comprueba la entrada de tres números iguales que pertenecen al intervalo permitido. Por lo que comprueba el mismo tipo de triángulo.

Recorrería el mismo camino en el grafo CFG.

##### 2. ¿Son necesarios los tests C2 y C3?

- *Test C2:* Sí, ya que se comprueba si se calcula correctamente que la suma de `a` y `b` sea menor que `c`.

- *Test C3:* Sí, porque comprueba que `c` no sea menor que 1.

##### 3. Dos posibles casos de prueba NO redundantes

- *Test C6:* Comprueba que un triángulo con medidas distintas, cada una dentro del intervalo y siendo válidas, es Escaleno.

>     @Test
>     public void testTipo_trianguloC6() {
>         a = 50;
>         b = 20;
>         c = 35;
>         resultadoEsperado = "Escaleno";
>         resultadoReal = tri.tipo_triangulo(a,b,c);
>         assertEquals(resultadoEsperado, resultadoReal);
>     }

- *Test C7:* Comprueba que la suma de `b` y `c` sea, estrictamente, menor que a.


>       @Test
>       public void testTipo_trianguloC7() {
>          a = 50;
>          b = 25;
>          c = 25;
>          resultadoEsperado = "No es un triangulo";
>          resultadoReal = tri.tipo_triangulo(a,b,c);
>          assertEquals(resultadoEsperado,resultadoReal);
>       }

### Ejercicio 3

#### A) Tabla de casos de prueba con 5 nuevos test NO redundantes

| Identificador del Caso de prueba *- Caminos -* | Dato de entrada 1 `edad` | Dato de entrada 2 `familiaNumerosa` | Dato de entrada 3 `repetidor` | Resultado esperado |
|------------------------------------------------|--------------------------|-------------------------------------|-------------------------------|--------------------|
| *C1*                                           | 23                       | true                                | true                          | 250.00             |
| *C2*                                           | 20                       | false                               | false                         | 500.00             |
| *C3*                                           | 19                       | false                               | true                          | 2000.00            |
| *C4*                                           | 45                       | true                                | true                          | 250.00             |
| *C5*                                           | 55                       | true                                | false                         | 400.00             |
| *C6*                                           | 66                       | false                               | false                         | 250.00             |

6 tests no son suficientes para comprobar que funciona correctamente.

#### B) Implementación de los casos de prueba

    float tasa = 500.00f;

    @Test
    public void testCalculaTasaMatriculaC2() {
        edad = 20;
        familiaNumerosa = false;
        repetidor = false;
        resultadoEsperado = tasa;
        resultadoReal = mat.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        //el tercer parámetro del método Assert.assertEquals es necesario si estamos comparando "floats"
        //en este caso el método devuelve cierto si:
        //resultadoEsperado = resultadoReal +/- 0.002
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }

    @Test
    public void testCalculaTasaMatriculaC3() {
        edad = 19;
        familiaNumerosa = false;
        repetidor = true;
        resultadoEsperado = tasa + 1500.00f;
        resultadoReal = mat.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        //el tercer parámetro del método Assert.assertEquals es necesario si estamos comparando "floats"
        //en este caso el método devuelve cierto si:
        //resultadoEsperado = resultadoReal +/- 0.002
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }

    @Test
    public void testCalculaTasaMatriculaC4() {
        edad = 45;
        familiaNumerosa = true;
        repetidor = true;
        resultadoEsperado = tasa / 2.00f;
        resultadoReal = mat.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        //el tercer parámetro del método Assert.assertEquals es necesario si estamos comparando "floats"
        //en este caso el método devuelve cierto si:
        //resultadoEsperado = resultadoReal +/- 0.002
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }

    @Test
    public void testCalculaTasaMatriculaC5() {
        edad = 55;
        familiaNumerosa = true;
        repetidor = false;
        resultadoEsperado = tasa - 100.00f;
        resultadoReal = mat.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        //el tercer parámetro del método Assert.assertEquals es necesario si estamos comparando "floats"
        //en este caso el método devuelve cierto si:
        //resultadoEsperado = resultadoReal +/- 0.002
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }

    @Test
    public void testCalculaTasaMatriculaC6() {
        edad = 66;
        familiaNumerosa = false;
        repetidor = false;
        resultadoEsperado = tasa / 2.00f;
        resultadoReal = mat.calculaTasaMatricula(edad,familiaNumerosa,repetidor);
        //el tercer parámetro del método Assert.assertEquals es necesario si estamos comparando "floats"
        //en este caso el método devuelve cierto si:
        //resultadoEsperado = resultadoReal +/- 0.002
        assertEquals(resultadoEsperado, resultadoReal,0.002f);
    }

Se ha encontrado un error en el cálculo de la tasa para un estudiante de entre 51 y 64 años de edad, que al mismo tiempo
es familia numerosa. Se ha solucionado cambiando los if de las líneas 11 y 13 por lo siguiente:

    if ((edad > 50) && (edad < 65)) {
        tasa = tasa - 100.00f;
    } else if ((familiaNumerosa) || (edad >= 65)) {
        tasa = tasa / 2;
    }

### Ejercicio 4

#### A) Ejecución de la fase *package* y cambios observados

Nuevos artefactos en el directorio *target*:

1. `clean`
    - Elimina el directorio *target*
    
2. `validate`
    - Escanea los ficheros (tengo que consultarlo con la profesora)
    
3. `compile`
    - classes/
    - generated-sources/
    - maven-status/
    
4. `test`
    - generated-test-sources/
    - surefire-reports/
    - test-clases/
   
5. `package`
    - maven-archiver/
    - P01-IntelliJ-1.0-SNAPSHOT.jar
   
En la fase `package` se ejecuta el plugin `jar:jar` por lo que se genera el archivo .jar del proyecto.

> **¡OJO!**
> 
> Si alguna de las fases anteriores, como `compile` o `test` no se ejecuta satisfactoriamente, no se pasa a la siguiente
> fase, por lo que no se llega a ejecutar la fase `package`.

#### B) Ejecución de la fase *install*

Se instalan dos ficheros en el repositorio local de Maven:

- P01-IntelliJ-1.0-SNAPSHOT.jar
- P01-IntelliJ-1.0-SNAPSHOT.pom

> **IMPORTANTE**
> 
> Para saber la ruta donde se ha instalado, dentro de la carpeta **$HOME/.m2/repository/**, hay que fijarse en las
> coordenadas del fichero *pom.xml* del proyecto. Que en este caso son las siguientes:
> 
>     <groupId>practica1.ppss</groupId>
>     <artifactId>P01-IntelliJ</artifactId>
>     <version>1.0-SNAPSHOT</version>
> 
> Por lo que la ruta, a partir de la anterior, será: **practica1/ppss/P01-IntelliJ/1.0-SNAPSHOT/**.