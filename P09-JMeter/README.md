# Soluciones

## Ejercicio 1

Estos son los parámetros la petición de login `/jpetstore/actions/Account.action-10`:

![Imagen parametros login][login_params]

## Ejercicio 2

### Listener Assertion Results

- **Sin fallos**:

![Imagen Assertion Results sin fallos][assert_results_sin]

- **Con fallos**:

![Imagen Assertion Results con fallos][assert_results_con]

### Listener View Results Tree

- **Sin fallos**:

![Imagen View Results Tree sin fallos][view_results_sin]

- **Con fallos**:

![Imagen View Results Tree con fallos][view_results_con]

### Listener hijos de algún sampler

#### Home

![Imagen Assertion Result en el sampler Home][assert_results_home]

#### Reptiles

![Imagen View Results Tree en el sampler Reptiles][view_results_reptiles]

### Resultados con pausas

Se ha configurado para que la ejecución sea un bucle de 3.

#### Aggregate Report

![Imagen Aggreate Report][aggregate]

#### Graph Results

![Imagen Graph][graph]

#### View Results in Table

![Imagen Table][table]

### Usuarios concurrentes

Rendimiento con 25 usuarios concurrentes.

#### Gráfica de Active Threads Over Time

![Imagen Active Threads Over Time][active_threads]

### Resultados Aggregate Report

![Imagen Aggregate Report 25 hilos concurrentes][aggregate25]

---

[login_params]: Images/login.jpg "Parametros del login"
[assert_results_sin]: Images/assertResultsSinFallos.jpg "Resultados del assert sin fallos"
[assert_results_con]: Images/assertResultsConFallos.jpg "Resultados del assert con fallos"
[view_results_sin]: Images/viewResultsSinFallos.jpg "Vista del árbol de peticiones sin fallos"
[view_results_con]: Images/viewResultsConFallos.jpg "Vista del árbol de peticiones con fallos"
[assert_results_home]: Images/HomeAssert.jpg "Resultado del assert en Home"
[view_results_reptiles]: Images/ReptilesView.jpg "Vista del árbol de peticiones de Reptiles"
[aggregate]: Images/Aggregate.jpg "Resultados de Aggregate Report"
[graph]: Images/Graph.jpg "Gráficos de ejecución con timers"
[table]: Images/Table.jpg "Tabla de resultados"
[active_threads]: Images/activeThreads.jpg "Gráfico de hilos activos"
[aggregate25]: Images/Aggregate25.jpg "Resultados de Aggregate Report con 25 hilos concurrentes"
